const express = require("express");
const { Sequelize, QueryTypes } = require("sequelize");
const { formatError, formatLocation } = require('../lib/data');

const initModels = require("../models/init-models");
const sequelize = new Sequelize(
  "mysql://root:root@localhost:3306/crowdlending"
);
const dbContext = initModels(sequelize);

const security = require("../lib/security");
const router = express.Router();

router.use(security.onlyAuthenticated)
router.use(security.onlyInRole('1'))

  router
    .route("/")
    .get(async function (req, res) { // sacar todos los proyectos en listado
      try {
        let resultado = await dbContext.proyecto.findAll();
        res.json(resultado).end();
      } catch (error) {
        res.status(400).json(formatError(error));
      }
    })

  router.route('/:id')
    .get(async function (req, res) { // seleccionar un proyecto
      try {
        let resultado = await dbContext.proyecto.findByPk(req.params.id);
        if (resultado) {
          res.json(resultado).end();
        } else
          res.sendStatus(404)
      } catch (error) {
        res.sendStatus(404)
      }
    })
    .put(async function (req, res) { // modificar
      if (req.body.id && req.body.id != req.params.id) {
        req.status(400).json({ message: 'Invalid identifier' })
        return
      }
      let row = await dbContext.proyecto.findByPk(req.params.id)
      if (!row) {
        res.sendStatus(404)
        return
      }
      row.set(req.body)
      try {
        await row.save()
        res.sendStatus(204)
      } catch (error) {
        res.status(400).send(formatError(error))
      }
    })

module.exports = router;
