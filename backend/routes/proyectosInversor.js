const express = require("express");
const { Sequelize, QueryTypes } = require("sequelize");
const { formatError, formatLocation } = require('../lib/data');

const initModels = require("../models/init-models");
const sequelize = new Sequelize(
  "mysql://root:root@localhost:3306/crowdlending"
);
const dbContext = initModels(sequelize);

const security = require("../lib/security");
const router = express.Router();

router.use(security.onlyAuthenticated)
router.use(security.onlyInRole('3'))

  router
    .route("/")
    .get(async function (req, res) { // sacar todos los proyectos en listado
      try {
        let resultado = await dbContext.proyecto.findAll();
        res.json(resultado).end();
      } catch (error) {
        res.status(400).json(formatError(error));
      }
    })

  router.route('/:id')
    .get(async function (req, res) { // seleccionar un proyecto
      try {
        let resultado = await dbContext.proyecto.findByPk(req.params.id);
        if (resultado) {
          res.json(resultado).end();
        } else
          res.sendStatus(404)
      } catch (error) {
        res.sendStatus(404)
      }
    })
    .put(async function (req, res) { // invertir
      if (req.body.id && req.body.id != req.params.id) {
        req.status(400).json({ message: 'Invalid identifier' })
        return
      }
      let row = await dbContext.proyecto.findByPk(req.params.id)
      if (!row) {
        res.sendStatus(404)
        return
      }
      row.estado = "ADJUDICADO";
      const inversion={idProyecto: req.params.id, idInversor: res.locals.usr, cuantia: req.body.nominal, estado: "ACEPTADA"};
      let item = await dbContext.inversion.create(inversion);

      try {
        await row.save()
        await item.save()
        res.status(200).json(row);
      } catch (error) {
        res.status(400).send(formatError(error))
      }
    });


module.exports = router;
