const express = require('express');
const { Sequelize, DataTypes, Op, QueryTypes } = require('sequelize');

const initModels = require("../models/Autenticacion/init-models");
const sequelize = new Sequelize('mysql://root:root@localhost:3306/Autenticacion')
const dbContext = initModels(sequelize);

var router = express.Router();

const security = require("../lib/security");


router.route('/')
    .get(async function (req, res) { // get all
        try {
            let resultado = await dbContext.Usuarios.findAll()
            res.json(resultado).end();
        } catch (error) {
            res.status(400).json(formatError(error))
        }
    })
router.route('/login')
    .get(function (req, res) {
        res.render('login', { user: "", password: "P@$$w0rd", intentos: 3 } )
    })
    .post(async function (req, res) {
        // { "user": "admin", "password": "P@$$w0rd"}
        if(req.body.user === "admin" && req.body.password === "P@$$w0rd") {
            if(req.query.continue)
            res.redirect(req.query.continue)
            else
            res.redirect('/')
        } else 
          res.render('login', {user: req.body.user, password: req.body.password, intentos: req.body.intentos-1} )
    })

router.route('/:correo')
    .get(async function (req, res) { // seleccionar un usuario 
      try {
        let resultado = await dbContext.Usuarios.findByPk((req.params.correo),{ where: { correo: res.locals.usr } });
        if (resultado) {
          res.json(resultado).end();
        } else
          res.sendStatus(404)
      } catch (error) {
        res.sendStatus(404)
      }
    })

module.exports = router;


