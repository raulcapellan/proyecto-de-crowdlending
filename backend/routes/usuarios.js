const express = require('express');
const { Sequelize, DataTypes, Op, QueryTypes } = require('sequelize');

const initModels = require("../models/init-models");
const sequelize = new Sequelize('mysql://root:root@localhost:3306/crowdlending')
const dbContext = initModels(sequelize);

var router = express.Router();

router.route('/')
    .get(async function (req, res) { // get all
        try {
            let resultado = await dbContext.usuario.findAll()
            res.json(resultado).end();
        } catch (error) {
            res.status(400).json(formatError(error))
        }
    })
router.route('/:correo')
    .get(async function (req, res) { // seleccionar un usuario
      try {
        let resultado = await dbContext.usuario.findByPk((req.params.correo),{ where: { correo: res.locals.usr } });
        if (resultado) {
          res.json(resultado).end();
        } else
          res.sendStatus(404)
      } catch (error) {
        res.sendStatus(404)
      }
    })

    .delete(async function (req, res) { // borrar usuario crowdlending
        let row = await dbContext.usuario.findByPk(req.params.correo)
        if (!row) {
          res.sendStatus(404)
          return
        }
        try {
          await row.destroy()
          res.sendStatus(204)
        } catch (error) {
          res.status(409).json(formatError(error, 409))
        }
      })
module.exports = router;

