const express = require("express");
const { Sequelize, QueryTypes } = require("sequelize");
const { formatError, formatLocation } = require('../lib/data');
const initModelsAutenticacion = require("../models/Autenticacion/init-models");

const initModels = require("../models/init-models");
const sequelize = new Sequelize(
  "mysql://root:root@localhost:3306/crowdlending"
);

const sequelizeAutenticacion= new Sequelize(
    "mysql://root:root@localhost:3306/Autenticacion"
  );

const dbContext = initModels(sequelize);
const dbContextAutenticacion = initModelsAutenticacion(sequelizeAutenticacion);
const security = require("../lib/security");
const router = express.Router();


router.use(security.onlyAuthenticated)
router.use(security.onlyInRole('3'))

router
  .route("/")
  .get(async function (req, res) { // obtener la información propia de cada promotor
    try {
      let resultado = await dbContext.inversor.findAll({ where: { correo: res.locals.usr } });
      res.json(resultado).end();
    } catch (error) {
      res.status(400).json(formatError(error));
    }
  })

  
  .post(async function (req, res) { // añadir un promotor
    let row = await dbContext.inversor.build(req.body)
    if (!row) {
      res.sendStatus(404)
      return
    }
    row.set(req.body)
    try {
      await row.save()
      res.sendStatus(204)
    } catch (error) {
      res.status(400).send(formatError(error))
    }
  })


router.route('/:correo')
  .get(async function (req, res) { // seleccionar un promotor
    try {
      let resultado = await dbContext.inversor.findByPk((req.params.correo),{ where: { correo: res.locals.usr } });
      if (resultado) {
        res.json(resultado).end();
      } else
        res.sendStatus(404)
    } catch (error) {
      res.sendStatus(404)
    }
  })
  .put(async function (req, res) { // modificar promotor
    if (req.body.correo && req.body.correo != req.params.correo) {
      req.status(400).json({ message: 'Invalid identifier' })
      return
    }
    let row = await dbContext.inversor.findByPk(req.params.correo)
    if (!row) {
      res.sendStatus(404)
      return
    }
    row.set(req.body)
    try {
      await row.save()
      res.sendStatus(204)
    } catch (error) {
      res.status(400).send(formatError(error))
    }
  })

    .get(async function (req, res) { // seleccionar un usuario
      try {
        let resultado = await dbContext.Usuarios.findByPk((req.params.correo),{ where: { correo: res.locals.usr } });
        if (resultado) {
          res.json(resultado).end();
        } else
          res.sendStatus(404)
      } catch (error) {
        res.sendStatus(404)
      }
    })

    .delete(async function (req, res) { // borrar usuario autenticación
        let row = await dbContextAutenticacion.Usuarios.findByPk(req.params.correo)
        if (!row) {
          res.sendStatus(404)
          return
        }
        try {
          await row.destroy()
          res.sendStatus(204)
        } catch (error) {
          res.status(409).json(formatError(error, 409))
        }
      })
    .delete(async function (req, res) { // borrar usuario crowdlending
        let row = await dbContext.usuario.findByPk(req.params.correo)
        if (!row) {
          res.sendStatus(404)
          return
        }
        try {
          await row.destroy()
          res.sendStatus(204)
        } catch (error) {
          res.status(409).json(formatError(error, 409))
        }
      })
      

    
  
    

module.exports = router;
