const express = require("express");
const { Sequelize, QueryTypes } = require("sequelize");
const { formatError, formatLocation } = require('../lib/data');

const initModels = require("../models/init-models");
const sequelize = new Sequelize(
  "mysql://root:root@localhost:3306/crowdlending"
);
const dbContext = initModels(sequelize);

const security = require("../lib/security");
const router = express.Router();

router.use(security.onlyAuthenticated)
router.use(security.onlyInRole('1'))

router
  .route("/")
.get(async function (req, res) {
    // Ver todos los promotores
    try {
      let resultado = await dbContext.promotor.findAll();
      res.json(resultado).end();
    } catch (error) {
      res.status(400).json(formatError(error));
    }
  })

router.route('/:correo')
  .get(async function (req, res) { // seleccionar un promotor
    try {
      let resultado = await dbContext.promotor.findByPk((req.params.correo),{ where: { correo: res.locals.usr } });
      if (resultado) {
        res.json(resultado).end();
      } else
        res.sendStatus(404)
    } catch (error) {
      res.sendStatus(404)
    }
  })
  .put(async function (req, res) { // modificar promotor
    if (req.body.correo && req.body.correo != req.params.correo) {
      req.status(400).json({ message: 'Invalid identifier' })
      return
    }
    let row = await dbContext.promotor.findByPk(req.params.correo)
    if (!row) {
      res.sendStatus(404)
      return
    }
    row.set(req.body)
    try {
      await row.save()
      res.sendStatus(204)
    } catch (error) {
      res.status(400).send(formatError(error))
    }
  })

module.exports = router;
