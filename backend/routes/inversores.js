const express = require("express");
const { Sequelize, DataTypes, Op, QueryTypes } = require("sequelize");
const { formatError, formatLocation } = require('../lib/data');
const bcrypt = require('bcryptjs')
const SALT_ROUNDS = 10;

const initModels = require("../models/init-models");
const initModelsAutenticacion = require("../models/Autenticacion/init-models");
const sequelize = new Sequelize(
  "mysql://root:root@localhost:3306/crowdlending"
);
const sequelizeAutenticacion= new Sequelize(
  "mysql://root:root@localhost:3306/Autenticacion"
);
const dbContext = initModels(sequelize);
const dbContextAutenticacion = initModelsAutenticacion(sequelizeAutenticacion);

var router = express.Router();

router
  .route("/")
  
  .post(async function (req, res) {
    let newUser = req.body.newUser;
    await insertOneInversor(newUser, res); // add
  });

  async function insertOneInversor(newUser, res) {
    try {
      await insertOneUser({
        id: newUser.correo,
        username: newUser.username,
        password: newUser.password,
      });
      newUser.estado = 'PENDIENTE';
      let item = await dbContext.inversor.create(newUser);
      await item.save();
      await insertOneUserAutenticacion({
        idUsuario: newUser.correo,
        nombre: newUser.username,
        password: await encriptaPassword(newUser.password),
        idRol: 3
      });
      res.sendStatus(201);
    } catch(error) {
  
      console.log(
        "No ha sido posible insertar al inversor " + newUser.correo + "." + error.message
      );
      await deleteOneUser(newUser.correo);
      res.status(400).send(formatError(error));
    }
  }

async function insertOneUser(newUser) {
  try {
    let item = await dbContext.usuario.create(newUser);
    await item.save();
  } catch(error) {
    console.log("No ha sido posible insertar al usuario " + newUser.id + "."  + error.message);
    throw error;
  }
}

module.exports = router;

async function deleteOneUser(pk) {
  try {
    let item = await dbContext.usuario.findByPk(pk);
    await item.destroy();
  } catch(error) {
    console.log("No ha sido posible eliminar al usuario " + pk + ".");
    throw error;
  }
}

async function insertOneUserAutenticacion(newUser) {
  try {
    let item = await dbContextAutenticacion.Usuarios.create(newUser);
    await item.save();
  } catch(error) {
    console.log("No ha sido posible insertar al Usuario Autenticacion " + newUser.idUsuario + ".");
    throw error;
  }
}

async function deleteOneUserAutenticacion(pk) {
  try {
    let item = await dbContextAutenticacion.Usuarios.findByPk(pk);
    await item.destroy();
  } catch(error) {
    console.log("No ha sido posible eliminar al Usuario Autenticacion " + pk + ".");
    throw error;
  }
}

async function encriptaPassword(password) {
  const salt = await bcrypt.genSalt(SALT_ROUNDS)
  const hash = await bcrypt.hash(password, salt)
  console.log(hash)
  return hash
}