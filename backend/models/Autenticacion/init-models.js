var DataTypes = require("sequelize").DataTypes;
var _Roles = require("./Roles");
var _Usuarios = require("./Usuarios");

function initModels(sequelize) {
  var Roles = _Roles(sequelize, DataTypes);
  var Usuarios = _Usuarios(sequelize, DataTypes);

  Usuarios.belongsTo(Roles, { as: "idRol_Role", foreignKey: "idRol"});
  Roles.hasMany(Usuarios, { as: "Usuarios", foreignKey: "idRol"});

  return {
    Roles,
    Usuarios,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
