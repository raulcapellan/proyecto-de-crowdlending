var DataTypes = require("sequelize").DataTypes;
var _comentarios = require("./comentarios");
var _inversion = require("./inversion");
var _inversor = require("./inversor");
var _promotor = require("./promotor");
var _proyecto = require("./proyecto");
var _usuario = require("./usuario");

function initModels(sequelize) {
  var comentarios = _comentarios(sequelize, DataTypes);
  var inversion = _inversion(sequelize, DataTypes);
  var inversor = _inversor(sequelize, DataTypes);
  var promotor = _promotor(sequelize, DataTypes);
  var proyecto = _proyecto(sequelize, DataTypes);
  var usuario = _usuario(sequelize, DataTypes);

  inversor.belongsToMany(proyecto, { as: 'idProyecto_proyectos', through: inversion, foreignKey: "idInversor", otherKey: "idProyecto" });
  proyecto.belongsToMany(inversor, { as: 'idInversor_inversors', through: inversion, foreignKey: "idProyecto", otherKey: "idInversor" });
  inversion.belongsTo(inversor, { as: "idInversor_inversor", foreignKey: "idInversor"});
  inversor.hasMany(inversion, { as: "inversions", foreignKey: "idInversor"});
  proyecto.belongsTo(promotor, { as: "creador_promotor", foreignKey: "creador"});
  promotor.hasMany(proyecto, { as: "proyectos", foreignKey: "creador"});
  inversion.belongsTo(proyecto, { as: "idProyecto_proyecto", foreignKey: "idProyecto"});
  proyecto.hasMany(inversion, { as: "inversions", foreignKey: "idProyecto"});
  comentarios.belongsTo(usuario, { as: "idAutor_usuario", foreignKey: "idAutor"});
  usuario.hasMany(comentarios, { as: "comentarios", foreignKey: "idAutor"});
  comentarios.belongsTo(usuario, { as: "idComentado_usuario", foreignKey: "idComentado"});
  usuario.hasMany(comentarios, { as: "idComentado_comentarios", foreignKey: "idComentado"});
  inversor.belongsTo(usuario, { as: "correo_usuario", foreignKey: "correo"});
  usuario.hasOne(inversor, { as: "inversor", foreignKey: "correo"});
  promotor.belongsTo(usuario, { as: "correo_usuario", foreignKey: "correo"});
  usuario.hasOne(promotor, { as: "promotor", foreignKey: "correo"});

  return {
    comentarios,
    inversion,
    inversor,
    promotor,
    proyecto,
    usuario,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
