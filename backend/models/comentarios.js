const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('comentarios', {
    idComentario: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    idAutor: {
      type: DataTypes.STRING(50),
      allowNull: false,
      references: {
        model: 'usuario',
        key: 'id'
      }
    },
    idComentado: {
      type: DataTypes.STRING(50),
      allowNull: false,
      references: {
        model: 'usuario',
        key: 'id'
      }
    },
    comentario: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    rating: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    creationDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    modificationDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'comentarios',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "idComentario" },
        ]
      },
      {
        name: "idComentado",
        using: "BTREE",
        fields: [
          { name: "idComentado" },
        ]
      },
      {
        name: "idAutor",
        using: "BTREE",
        fields: [
          { name: "idAutor" },
        ]
      },
    ]
  });
};
