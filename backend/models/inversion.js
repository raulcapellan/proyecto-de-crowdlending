const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('inversion', {
    idProyecto: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'proyecto',
        key: 'id'
      }
    },
    idInversor: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'inversor',
        key: 'correo'
      }
    },
    cuantia: {
      type: DataTypes.DECIMAL(9,2),
      allowNull: false
    },
    creationDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    modificationDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    estado: {
      type: DataTypes.STRING(20),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'inversion',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "idProyecto" },
          { name: "idInversor" },
        ]
      },
      {
        name: "idProyecto",
        using: "BTREE",
        fields: [
          { name: "idProyecto" },
        ]
      },
      {
        name: "idInversor",
        using: "BTREE",
        fields: [
          { name: "idInversor" },
        ]
      },
    ]
  });
};
