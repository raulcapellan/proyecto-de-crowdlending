const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('promotor', {
    correo: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'usuario',
        key: 'id'
      }
    },
    nombre: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    apellido1: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    apellido2: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    nif: {
      type: DataTypes.STRING(9),
      allowNull: false,
      unique: "nif_UNIQUE"
    },
    telefono: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    direccion: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    cp: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    ciudad: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    pais: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    iban: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    profesion: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    estado: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    saldo: {
      type: DataTypes.DECIMAL(9,2),
      allowNull: true
    },
    rating: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    creationDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    modificationDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'promotor',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "correo" },
        ]
      },
      {
        name: "nif_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "nif" },
        ]
      },
      {
        name: "correo_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "correo" },
        ]
      },
    ]
  });
};
