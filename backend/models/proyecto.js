const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('proyecto', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nombre: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    descripcion: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    tipo: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    nominal: {
      type: DataTypes.DECIMAL(9, 2),
      allowNull: false,
      validate: {
        esPositivo(valor) {
          if (valor < 0)
            throw new Error('Debe ser positivo');
        }
      }
    },
    interes: {
      type: DataTypes.DECIMAL(4, 2),
      allowNull: false,
      validate: {
        esPositivo(valor) {
          if (valor < 0)
            throw new Error('Debe ser positivo');
        }
      }

    },
    plazos: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        esPositivo(valor) {
          if (valor < 0)
            throw new Error('Debe ser positivo');
        }
      }
    },
    rating: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    imagen: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    estado: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    creador: {
      type: DataTypes.STRING(50),
      allowNull: false,
      references: {
        model: 'promotor',
        key: 'correo'
      }
    },
    creationDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    modificationDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'proyecto',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "creador",
        using: "BTREE",
        fields: [
          { name: "creador" },
        ]
      },
    ]
  });
};
