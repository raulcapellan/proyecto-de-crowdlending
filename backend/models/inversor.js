const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('inversor', {
    correo: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'usuario',
        key: 'id'
      }
    },
    nombre: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    apellido1: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    apellido2: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    nif: {
      type: DataTypes.STRING(9),
      allowNull: false,
      unique: "nif_UNIQUE"
    },
    telefono: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    direccion: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    cp: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    ciudad: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    pais: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    profesion: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    estado: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    experiencia: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    iban: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    creationDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    modificationDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'inversor',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "correo" },
        ]
      },
      {
        name: "nif_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "nif" },
        ]
      },
    ]
  });
};
