var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var seguridad = require('./routes/seguridad')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/usuarios');
var usersRter = require('./routes/users');
var inversoresRouter = require('./routes/inversores');
var inversoresRouterInv = require('./routes/inversoresInversor');
var inversoresRouterGest = require('./routes/inversoresGest');
var proyectosRouterGestor = require('./routes/proyectosGestor');
var proyectosRouterInversor = require('./routes/proyectosInversor');
var proyectosRouterPromotor = require('./routes/proyectosPromotor');
var promotoresRouter = require('./routes/promotores');
var promotoresRouterProm = require('./routes/promotoresPromotor');
var promotoresRouterGest = require('./routes/promotoresGest')


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
  var origen = req.header("Origin")
  if (!origen) origen = '*'
  res.header('Access-Control-Allow-Origin', origen)
  res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, X-Requested-With, X-XSRF-TOKEN')
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
  res.header('Access-Control-Allow-Credentials', 'true')
  next()
})

app.use('/',seguridad);
app.use('/', indexRouter);
app.use('/usuarios/v1', usersRouter);
app.use('/inversores/v1', inversoresRouter);
app.use('/inv/v1', inversoresRouterInv);
app.use('/invGest/v1', inversoresRouterGest);
app.use('/inversor/proyectos/v1', proyectosRouterInversor);
app.use('/gestor/proyectos/v1', proyectosRouterGestor);
app.use('/promotor/proyectos/v1', proyectosRouterPromotor);
app.use('/promotores/v1', promotoresRouter);
app.use('/promot/v1', promotoresRouterProm);
app.use('/promotGest/v1', promotoresRouterGest);
app.use('/users/v1',usersRter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
