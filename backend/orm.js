const { Sequelize, DataTypes, Op, QueryTypes } = require("sequelize");
const initModels = require("../backend/models/init-models");
const sequelize = new Sequelize(
  "mysql://root:root@localhost:3306/crowdlending"
);
const dbContext = initModels(sequelize);

console.log(`Inicio ${new Date().toLocaleTimeString("es")}`);

async function allInvestors() {
  let rows = await dbContext.inversor.findAll();
  rows.forEach((row) => {
    console.log(row.toJSON());
  });
}

async function findUserById(mail) {
  let item = await dbContext.usuarios.findOne({
    where: { correo: { [Op.eq]: mail } },
  });

  if (item) {
    console.log(item.toJSON());
  } else {
    console.log("Usuario " + mail + " no encontrado.");
  }
}


async function insertOneInversor(newUser) {
  try {
    await insertOneUser({id: newUser.correo, username: newUser.username, password: newUser.password})
    let item = await dbContext.inversor.create(newUser);
    await item.save();
  } catch {
    console.log(
      "No ha sido posible insertar al inversor " + newUser.correo + "."
    );
  }
}

async function insertOneUser(newUser) {
    try {
      let item = await dbContext.usuario.create(newUser);
      await item.save();
    } catch {
      console.log(
        "No ha sido posible insertar al usuario " + newUser.id + "."
      );
    }
  }

  async function deleteOneUser(newUser) {
    try {
      await dbContext.usuario.destroy({where: {id: newUser.correo}});
    } catch {
      console.log(
        "No ha sido posible eliminar al usuario " + newUser.correo + "."
      );
    }
  }

async function insertMany(list) {
    list.forEach((row) => {
      insertOneInversor(row);
    });
  }

  async function deleteManyUsers(list) {
    list.forEach((row) => {
      deleteOneUser(row);
    });
  }



  let list = [
    {
      "nombre": "Aimil",
      "apellido1": "Beacham",
      "apellido2": "Nilges",
      "dni": "18561508W",
      "correo": "anilges0@deliciousdays.com",
      "telefono": "588-486-2209",
      "direccion": "47604 Manitowish Terrace",
      "cp": "456113",
      "ciudad": "Katav-Ivanovsk",
      "pais": "Russia",
      "profesion": "Senior Cost Accountant",
      "estado": "PENDIENTE",
      "experiencia": "NOVEL",
      "iban": "LT42 0912 3760 4644 3433",
      "username": "anilges0",
      "password": "lH8FQZ"
    },
    {
      "nombre": "Jillane",
      "apellido1": "Creeghan",
      "apellido2": "Fairbourn",
      "dni": "78884227T",
      "correo": "jfairbourn1@spotify.com",
      "telefono": "928-999-5179",
      "direccion": "8263 Texas Street",
      "cp": "357859",
      "ciudad": "Galyugayevskaya",
      "pais": "Russia",
      "profesion": "Recruiter",
      "estado": "PENDIENTE",
      "experiencia": "EXPERTO",
      "iban": "PL86 5771 9927 6369 3241 0378 0689",
      "username": "jfairbourn1",
      "password": "cNrtbI4"
    },
    {
      "nombre": "Andi",
      "apellido1": "Shildrake",
      "apellido2": "MacTrusty",
      "dni": "41191964E",
      "correo": "amactrusty2@noaa.gov",
      "telefono": "575-677-7209",
      "direccion": "8823 Kinsman Hill",
      "ciudad": "Quận Mười",
      "pais": "Vietnam",
      "profesion": "Engineer II",
      "estado": "PENDIENTE",
      "experiencia": "EXPERTO",
      "iban": "MC59 4854 7629 57YY KKU2 A1PN F60",
      "username": "amactrusty2",
      "password": "oeyWFKcF"
    },
    {
      "nombre": "Nichol",
      "apellido1": "Allonby",
      "apellido2": "Nehlsen",
      "dni": "67137440A",
      "correo": "nnehlsen3@bluehost.com",
      "telefono": "244-647-0171",
      "direccion": "21 Sheridan Junction",
      "cp": "13456 C",
      "ciudad": "Marseille",
      "pais": "France",
      "profesion": "Assistant Media Planner",
      "estado": "PENDIENTE",
      "experiencia": "NOVEL",
      "iban": "MD56 MEBO MHIH RMYK NVUN 8EOJ",
      "username": "nnehlsen3",
      "password": "yJt7WA"
    },
    {
      "nombre": "Nolie",
      "apellido1": "Arthan",
      "apellido2": "Priestman",
      "dni": "40026693T",
      "correo": "npriestman4@hhs.gov",
      "telefono": "454-442-7615",
      "direccion": "0 Menomonie Lane",
      "cp": "8311",
      "ciudad": "Kabuynan",
      "pais": "Philippines",
      "profesion": "Financial Advisor",
      "estado": "CANCELADO",
      "experiencia": "NOVEL",
      "iban": "BG03 SSZI 2090 55QA EDHP 3J",
      "username": "npriestman4",
      "password": "4gfQFQ5jMm"
    },
    {
      "nombre": "Anthiathia",
      "apellido1": "Nansom",
      "apellido2": "Starkings",
      "dni": "84011079Z",
      "correo": "astarkings5@sogou.com",
      "telefono": "709-112-8508",
      "direccion": "65 Arizona Pass",
      "ciudad": "Sanyantang",
      "pais": "China",
      "profesion": "Recruiter",
      "estado": "PENDIENTE",
      "experiencia": "EXPERTO",
      "iban": "KZ62 794I ROHZ KHEY RRPI",
      "username": "astarkings5",
      "password": "MTtzbG"
    },
    {
      "nombre": "Corinna",
      "apellido1": "Dacre",
      "apellido2": "Hallbord",
      "dni": "10096258V",
      "correo": "challbord6@mlb.com",
      "telefono": "461-872-2621",
      "direccion": "19 Derek Street",
      "ciudad": "Chenqiao",
      "pais": "China",
      "profesion": "Food Chemist",
      "estado": "PENDIENTE",
      "experiencia": "EXPERTO",
      "iban": "PK54 PFOZ ENRH B8HW UQIR AO6H",
      "username": "challbord6",
      "password": "4gWYlF"
    },
    {
      "nombre": "Katha",
      "apellido1": "Lindner",
      "apellido2": "Sawdon",
      "dni": "53480173K",
      "correo": "ksawdon7@hc360.com",
      "telefono": "629-212-2824",
      "direccion": "06 Nobel Junction",
      "cp": "242467",
      "ciudad": "Ursk",
      "pais": "Russia",
      "profesion": "Systems Administrator I",
      "estado": "CANCELADO",
      "experiencia": "EXPERTO",
      "iban": "MU43 UULV 4163 7750 6776 1767 634L JZ",
      "username": "ksawdon7",
      "password": "5u34aUay"
    },
    {
      "nombre": "Frank",
      "apellido1": "Howden",
      "apellido2": "Pressdee",
      "dni": "05350069Q",
      "correo": "fpressdee8@bing.com",
      "telefono": "196-403-8091",
      "direccion": "3 Londonderry Point",
      "ciudad": "Sorong",
      "pais": "Indonesia",
      "profesion": "Senior Editor",
      "estado": "CANCELADO",
      "experiencia": "NOVEL",
      "iban": "BH13 OJXA CMNY XTUL KAXO P6",
      "username": "fpressdee8",
      "password": "Fc7hfVPae"
    },
    {
      "nombre": "Magnum",
      "apellido1": "Hayer",
      "apellido2": "Coultard",
      "dni": "70600768Z",
      "correo": "mcoultard9@engadget.com",
      "telefono": "980-502-7750",
      "direccion": "84 Meadow Valley Pass",
      "ciudad": "Dakoro",
      "pais": "Niger",
      "profesion": "Account Representative III",
      "estado": "PENDIENTE",
      "experiencia": "EXPERTO",
      "iban": "LV04 KRBH SOSK CU85 LUU0 Q",
      "username": "mcoultard9",
      "password": "VabjniTHw"
    }
  ];
  

insertMany(list);








