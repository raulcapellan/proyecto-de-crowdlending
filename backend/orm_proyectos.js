const { Sequelize, DataTypes, Op, QueryTypes } = require("sequelize");
const initModels = require("../backend/models/init-models");
const sequelize = new Sequelize(
  "mysql://root:root@localhost:3306/crowdlending"
);
const dbContext = initModels(sequelize);

console.log(`Inicio ${new Date().toLocaleTimeString("es")}`);

async function allProyectos() {
  let rows = await dbContext.proyecto.findAll();
  rows.forEach((row) => {
    console.log(row.toJSON());
  });
}

async function conUno(correoprom) {
  let row = await dbContext.promotor.findOne({ where: { correo: { [Op.eq]: correoprom } } })
  // let row = await dbContext.actor.findByPk(1)
  return row.toJSON()
}

async function conMuchos() {
    let rows = await dbContext.Usuarios.findAll({ where: { idProyecto: { [Op.gt]: 1} } })
    rows.forEach(row => {
        console.log(row.toJSON())
    });
}

async function findUserById(mail) {
  let item = await dbContext.usuarios.findOne({
    where: { correo: { [Op.eq]: mail } },
  });

  if (item) {
    console.log(item.toJSON());
  } else {
    console.log("Usuario " + mail + " no encontrado.");
  }
}


async function insertOneProyecto(newProyecto) {
  try {
    let item = await dbContext.proyecto.create(newProyecto);
    await item.save();
  } catch (error){
    console.log(
      "No ha sido posible insertar el proyecto " + newProyecto.nombre + "." + " " + error
    );
  }
}

async function insertOneUser(newUser) {
    try {
      let item = await dbContext.usuario.create(newUser);
      await item.save();
    } catch {
      console.log(
        "No ha sido posible insertar al usuario " + newUser.id + "."
      );
    }
  }

  async function deleteOneUser(newUser) {
    try {
      await dbContext.usuario.destroy({where: {id: newUser.correo}});
    } catch {
      console.log(
        "No ha sido posible eliminar al usuario " + newUser.correo + "."
      );
    }
  }

async function insertMany(list) {
    list.forEach((row) => {
      insertOneProyecto(row);
    });
  }

  async function deleteManyUsers(list) {
    list.forEach((row) => {
      deleteOneUser(row);
    });
  }



  let list = 
  
  [{"creador":"tkinton0@economist.com","descripcion":"Chrysler","estado":"Borrador","imagen":"https://picsum.photos/200/300","interes":13.5,"nombre":"Sebring","nominal":"284665.37","plazos":71,"rating":"AA","tipo":"Coche","creationDate":"2021-12-26 08:43:02","modificationDate":"2022-03-12 03:40:47"},
  {"creador":"sroddell1@accuweather.com","descripcion":"Suzuki","estado":"Aprobado","imagen":"https://picsum.photos/200/300","interes":38.1,"nombre":"X-90","nominal":"486804.34","plazos":71,"rating":"BB-","tipo":"Coche","creationDate":"2021-10-10 13:46:02","modificationDate":"2022-09-20 09:22:57"},
  {"creador":"ssheldrake2@java.com","descripcion":"Volkswagen","estado":"Aprobado","imagen":"https://picsum.photos/200/300","interes":1.15,"nombre":"Jetta","nominal":"1535.13","plazos":5,"rating":"AAA+","tipo":"Coche","creationDate":"2021-06-29 10:22:47","modificationDate":"2022-02-02 04:53:30"},
  {"creador":"cbasili3@eventbrite.com","descripcion":"Audi","estado":"Borrador","imagen":"https://picsum.photos/200/300","interes":79.37,"nombre":"riolet","nominal":"862857.05","plazos":3,"rating":"CCC","tipo":"Coche","creationDate":"2021-10-17 08:07:18","modificationDate":"2022-04-27 05:07:25"},
  {"creador":"alaffin4@gov.uk","descripcion":"Cadillac","estado":"Borrador","imagen":"https://picsum.photos/200/300","interes":91.48,"nombre":"Eldorado","nominal":"495364.44","plazos":51,"rating":"D","tipo":"Coche","creationDate":"2021-09-06 10:27:40","modificationDate":"2022-06-01 00:34:47"},
  {"creador":"wbuttle5@washington.edu","descripcion":"Pontiac","estado":"Aprobado","imagen":"https://picsum.photos/200/300","interes":70.57,"nombre":"LeMans","nominal":"298081.67","plazos":87,"rating":"CC","tipo":"Coche","creationDate":"2021-09-08 07:45:19","modificationDate":"2022-03-21 21:33:26"},
  {"creador":"pboylin6@wikimedia.org","descripcion":"Acura","estado":"Borrador","imagen":"https://picsum.photos/200/300","interes":62.11,"nombre":"Legend","nominal":"188784.19","plazos":40,"rating":"C","tipo":"Coche","creationDate":"2021-12-26 14:24:27","modificationDate":"2022-05-02 18:49:41"},
  {"creador":"wfantini7@ask.com","descripcion":"Dodge","estado":"Cancelado","imagen":"https://picsum.photos/200/300","interes":3.69,"nombre":"Ram Van 2500","nominal":"757940.87","plazos":93,"rating":"AAA+","tipo":"Coche","creationDate":"2021-09-01 22:49:36","modificationDate":"2022-11-26 22:23:35"},
  {"creador":"edunniom8@issuu.com","descripcion":"Mitsubishi","estado":"Borrador","imagen":"https://picsum.photos/200/300","interes":14.79,"nombre":"Galant","nominal":"503957.04","plazos":23,"rating":"A","tipo":"Coche","creationDate":"2021-09-27 02:37:56","modificationDate":"2022-04-01 04:04:01"},
  {"creador":"rlescop9@admin.ch","descripcion":"Pontiac","estado":"Adjudicado","imagen":"https://picsum.photos/200/300","interes":27.1,"nombre":"Grand Prix","nominal":"202391.37","plazos":48,"rating":"BBB+","tipo":"Coche","creationDate":"2021-10-11 07:01:23","modificationDate":"2022-07-11 04:13:33"}];
  

// insertMany(list);
//insertOneProyecto(list[0]);
insertOneProyecto(newProyecto);



