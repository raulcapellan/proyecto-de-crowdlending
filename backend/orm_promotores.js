const { Sequelize, DataTypes, Op, QueryTypes } = require("sequelize");
const initModels = require("../backend/models/init-models");
const sequelize = new Sequelize(
  "mysql://root:root@localhost:3306/crowdlending"
);
const dbContext = initModels(sequelize);

console.log(`Inicio ${new Date().toLocaleTimeString("es")}`);

async function allPromotores() {
  let rows = await dbContext.inversor.findAll();
  rows.forEach((row) => {
    console.log(row.toJSON());
  });
}

async function conUno(correoprom) {
  let row = await dbContext.promotor.findOne({ where: { correo: { [Op.eq]: correoprom } } })
  // let row = await dbContext.actor.findByPk(1)
  return row.toJSON()
}
async function findUserById(correo) {
  let item = await dbContext.promotor.findOne({
    where: { correo: { [Op.eq]: correo } },
  });

  if (item) {
    console.log(item.toJSON());
  } else {
    console.log("Promotor " + correo + " no encontrado.");
  }
}


async function insertOnePromotor(newUser) {
  try {
    await insertOneUser({id: newUser.correo, username: newUser.username, password: newUser.password, creationDate: new Date(), modificationDate: new Date()})
    let item = await dbContext.promotor.create(newUser);
    await item.save();
  } catch {
    console.log(
      "No ha sido posible insertar al promotor " + newUser.correo + "."
    );
  }
}

async function insertOneUser(newUser) {
    try {
      let item = await dbContext.usuario.create(newUser);
      await item.save();
    } catch {
      console.log(
        "No ha sido posible insertar al usuario " + newUser.id + "."
      );
    }
  }

  async function deleteOneUser(newUser) {
    try {
      await dbContext.usuario.destroy({where: {id: newUser.correo}});
    } catch {
      console.log(
        "No ha sido posible eliminar al usuario " + newUser.correo + "."
      );
    }
  }

async function insertMany(list) {
    list.forEach((row) => {
      insertOnePromotor(row);
    });
  }

  async function deleteManyUsers(list) {
    list.forEach((row) => {
      deleteOneUser(row);
    });
  }



  let list = 
    [{
      "apellido1": "Matt",
      "apellido2": "Kinton",
      "ciudad": "Chahār Burj",
      "correo": "tkinton0@economist.com",
      "cp": "75146",
      "creationDate": "2020-11-12 13:59:59",
      "direccion": "777 Lighthouse Bay Court",
      "estado": "pendiente",
      "iban": "FR43 2583 9875 20HU LKVV KMLR X49",
      "modificationDate": "2021-11-12 13:59:59",
      "nif": "28639536M",
      "nombre": "Timothee",
      "pais": "Afghanistan",
      "profesion": "Electrical Engineer",
      "rating": "AA",
      "telefono": "376-245-6444",
      "password": "vwy1ZRZ",
      "username": "tkinton0",
      "saldo": "38",
    }, {
      "apellido1": "Risbridger",
      "apellido2": "Roddell",
      "ciudad": "Cool űrhajó",
      "correo": "sroddell1@accuweather.com",
      "cp": "30076",
      "creationDate": "9/05/1997",
      "direccion": "20048 Prairie Rose Hill",
      "estado": "cancelado",
      "iban": "FR38 6667 1357 57WC MNLN R002 666",
      "modificationDate": "9/18/2020",
      "nif": "99805470Y",
      "nombre": "Sebastian",
      "pais": "Afghanistan",
      "profesion": "VP Product Management",
      "rating": "AA",
      "telefono": "964-435-9876",
      "password": "iRUucY",
      "username": "sroddell1",
      "saldo": "2"
    }, {
      "apellido1": "Fido",
      "apellido2": "Sheldrake",
      "ciudad": "Shīnḏanḏ",
      "correo": "ssheldrake2@java.com",
      "cp": "28833",
      "creationDate": "3/25/1999",
      "direccion": "3 Marcy Crossing",
      "estado": "pendiente",
      "iban": "IE37 OGFU 4384 9326 8369 40",
      "modificationDate": "8/12/2001",
      "nif": "02072230E",
      "nombre": "Shayna",
      "pais": "Afghanistan",
      "profesion": "Cost Accountant",
      "rating": "A",
      "telefono": "937-793-5155",
      "password": "bGvxUaQ",
      "username": "ssheldrake2",
      "saldo": "43"
    }, {
      "apellido1": "Jickles",
      "apellido2": "Basili",
      "ciudad": "Chowṉêy",
      "correo": "cbasili3@eventbrite.com",
      "cp": "04155",
      "creationDate": "12/12/2012",
      "direccion": "2742 Burrows Junction",
      "estado": "pendiente",
      "iban": "MC09 0370 4634 67NZ JCKU VRPM G61",
      "modificationDate": "8/10/2020",
      "nif": "91083617Y",
      "nombre": "Curry",
      "pais": "Afghanistan",
      "profesion": "Assistant Professor",
      "rating": "A",
      "telefono": "382-944-6887",
      "password": "Ocy1rAGfwoN",
      "username": "cbasili3",
      "saldo": "90"
    }, {
      "apellido1": "Britney",
      "apellido2": "Laffin",
      "ciudad": "Shīnḏanḏ",
      "correo": "alaffin4@gov.uk",
      "cp": "94426",
      "creationDate": "2/05/1998",
      "direccion": "9474 Namekagon Parkway",
      "estado": "pendiente",
      "iban": "GR04 8073 6801 XXGU RF9X NIHD PFU",
      "modificationDate": "8/02/2021",
      "nif": "10040703F",
      "nombre": "Augustin",
      "pais": "Afghanistan",
      "profesion": "Operator",
      "rating": "AAA",
      "telefono": "382-819-4613",
      "password": "G0MipVLDqd",
      "username": "alaffin4",
      "saldo": "64"
    }, {
      "apellido1": "Gracewood",
      "apellido2": "Buttle",
      "ciudad": "Khayr Kōṯ",
      "correo": "wbuttle5@washington.edu",
      "cp": "69753",
      "creationDate": "3/01/2011",
      "direccion": "57253 Dovetail Junction",
      "estado": "aprobado",
      "iban": "FR74 1018 6511 87HQ NBDT W0PU B72",
      "modificationDate": "8/12/2017",
      "nif": "57438084T",
      "nombre": "Weider",
      "pais": "Afghanistan",
      "profesion": "Account Coordinator",
      "rating": "AA",
      "telefono": "509-732-9371",
      "password": "b8iZD3OZHSt",
      "username": "wbuttle5",
      "saldo": "50"
    }, {
      "apellido1": "Klulisek",
      "apellido2": "Boylin",
      "ciudad": "Kalān Deh",
      "correo": "pboylin6@wikimedia.org",
      "cp": "62777",
      "creationDate": "3/02/2000",
      "direccion": "30 Delaware Park",
      "estado": "pendiente",
      "iban": "FR53 2824 2614 80IP YEPQ JN12 C32",
      "modificationDate": "1/12/2022",
      "nif": "37172170F",
      "nombre": "Portia",
      "pais": "Afghanistan",
      "profesion": "Research Nurse",
      "rating": "B",
      "telefono": "955-590-0318",
      "password": "L7wRLIen",
      "username": "pboylin6",
      "saldo": "24"
    }, {
      "apellido1": "Duddell",
      "apellido2": "Fantini",
      "ciudad": "Khōst",
      "correo": "wfantini7@ask.com",
      "cp": "87189",
      "creationDate": "2/11/2012",
      "direccion": "632 Springview Way",
      "estado": "pendiente",
      "iban": "FR25 3059 1203 30IF PF0G Z0CZ S67",
      "modificationDate": "1/10/2021",
      "nif": "34821588W",
      "nombre": "Winne",
      "pais": "Afghanistan",
      "profesion": "VP Quality Control",
      "rating": "AAA",
      "telefono": "821-400-1579",
      "password": "sPeDLYOh",
      "username": "wfantini7",
      "saldo": "425.1"
    }, {
      "apellido1": "Husset",
      "apellido2": "Dunniom",
      "ciudad": "Kalān Deh",
      "correo": "edunniom8@issuu.com",
      "cp": "03964",
      "creationDate": "6/12/2020",
      "direccion": "9964 Mesta Way",
      "estado": "cancelado",
      "iban": "GI80 BSXX CYTC I9PG MB3E 4ES",
      "modificationDate": "8/12/2021",
      "nif": "89090520C",
      "nombre": "Elna",
      "pais": "Afghanistan",
      "profesion": "Developer I",
      "rating": "A",
      "telefono": "617-314-0414",
      "password": "tgebgbj2mE",
      "username": "edunniom8",
      "saldo": "25589"
    }, {
      "apellido1": "Dunklee",
      "apellido2": "Lescop",
      "ciudad": "Ḩukūmat-e Shīnkaī",
      "correo": "rlescop9@admin.ch",
      "cp": "51627",
      "creationDate": "5/16/2018",
      "direccion": "958 Northport Avenue",
      "estado": "pendiente",
      "iban": "RS47 3382 7444 3330 3578 92",
      "modificationDate": "5/12/2021",
      "nif": "53532896M",
      "nombre": "Roger",
      "pais": "Afghanistan",
      "profesion": "Staff Accountant IV",
      "rating": "B",
      "telefono": "147-694-4516",
      "password": "E0SMQ6zQuLhK",
      "username": "rlescop9",
      "saldo": "951443"
    }];
   
//module.exports.findUserById = findUserById
//insertMany(list);
//findUserById(correo)
module.exports.conUno=conUno