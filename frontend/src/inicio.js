import { Link } from "react-router-dom";
import "./inicio.css";
import { useSelector } from 'react-redux';
import { selectIsAuthenticated, selectIsInRole } from './store/auth-slice'
import MenuGestor from "./menugestor";
import MenuInversor from "./menuinversor";
import MenuPromotor from "./menupromotor";
import MenuUsuarioNormal from "./menuusuarionormal";


function Principal(props) {


    return (

      <>


        <div className="cuerpo">
          <p>
          Nuestras opciones de inversión totalmente automatizadas están diseñadas para ahorrarle tiempo y garantizar que no se pierda ninguna oportunidad que le atraiga al hacer que su cuenta sea tan fácil como desee. Con una opción entre configuraciones conservadoras, equilibradas y personalizadas, puede adaptar su estrategia para satisfacer perfectamente sus necesidades de inversión.
          </p>
          <h2 className="subtitulo">¿Por qué invertir con nosotros?</h2>
          <p>Puedes llevar tu cuenta de Estateguru contigo a cualquier parte, en cualquier dispositivo. Asegúrese de no perder nunca una gran oportunidad de inversión con la capacidad de administrar su tablero sobre la marcha. Es como tener una cartera de inversiones en el bolsillo.</p>
          <p>Obtener las mejores ofertas tiene que ver con el tiempo y el conocimiento, por lo que le permitimos ajustar la cantidad de información que le enviamos hasta el más mínimo detalle. Ya sea que desee conocer cada oportunidad de inversión a medida que surja, simplemente prefiera actualizaciones más amplias o cualquier otra cosa, lo tenemos cubierto.</p>
          <div className="imagenInicio">
            <img
              src={"https://picsum.photos/id/128/900/300"}
              alt="Lago entre montañas"
            ></img>
          </div>
        </div>

      </>
    );

}
export default Principal;
