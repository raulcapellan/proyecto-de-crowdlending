import React, {Component} from 'react';
import { ValidationMessage } from './comunes';
import { esNIF} from './DNI'
import { fnValidateIBAN} from './IBAN'
import {checkPassword} from './contraseña'
import { Link } from "react-router-dom";
import axios from 'axios';
import MenuInversor from './menuinversor';
export default class FormularioInversores extends Component{
    constructor(props) {
        super(props)
        this.state = {
            elemento: {
                id: 0,
                correo: '',
                nombre: '',
                apellido1: '',
                apellido2: '',
                nif: '',
                telefono: '',
                pais: '',
                ciudad: '',
                direccion: '',
                cp: '',
                iban: '',
                experiencia: '',
                profesion: '',
                password:'',
                username:''
            },
            esInvalido: false,
            errores: {}
        }
        this.form = React.createRef();
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let cmp = event.target.name
        let value = event.target.value
        this.setState(prev => {
            let elemento = this.state.elemento;
            elemento[cmp] = value
            return { elemento }
        });
        this.validate()
    }
    
    componentDidMount() {
        this.validate()
    }
    validate() {
        if (this.form) {
            let esInvalido = false
            let errores = {}
            for (let cntr of this.form.current.elements) {
                if (cntr.name) {
                    // eslint-disable-next-line default-case
                    switch (cntr.name) {
                        case 'nif':
                            if (cntr.value && !esNIF(cntr.value))
                                cntr.setCustomValidity('No es un DNI valido')
                            else
                                cntr.setCustomValidity('')
                            break;

                        case 'iban':
                            if (cntr.value && !fnValidateIBAN(cntr.value))
                                cntr.setCustomValidity('No es un IBAN válido')
                            else
                                cntr.setCustomValidity('')
                            break;
                        case 'password':
                            if (cntr.value && !checkPassword(cntr.value))
                                    cntr.setCustomValidity('No es una contraseña válida')
                                else
                                    cntr.setCustomValidity('')
                                break;

                    }
                    errores[cntr.name] = cntr.validationMessage;
                    if(!cntr.validity.valid) esInvalido = true
                }
            }
            this.setState({ errores, esInvalido });
        }
    }

    render() {
         return (
            <>
            <form ref={this.form}>
                <div className="container-fluid">
                <div className="seccionFormulario">Datos personales</div>
                <div className="row">
                    <div className="col">
                    <label htmlFor='nombre'>Nombre:</label>
                    <input type='text' className= 'form-control' id='nombre' name='nombre' value={this.state.elemento.nombre} onChange={this.handleChange}
                        required  minLength={2}/>
                    <ValidationMessage msg={this.state.errores.nombre} />
                    </div>
                    <br />
                    <div className="col">
                    <label htmlFor='apellido1'>Primer Apellido:</label>
                    <input type='text' className= 'form-control' id='apellido1' name='apellido1' value={this.state.elemento.primerapellido} onChange={this.handleChange}
                        required minLength={2}/>
                    <ValidationMessage msg={this.state.errores.apellido1} />
                    </div>
                    <br />
                    <div className="col">
                    <label htmlFor='apellido2'>Segundo Apellido:</label>
                    <input type='text' className= 'form-control' id='apellido2' name='apellido2' value={this.state.elemento.segundoapellido} onChange={this.handleChange}
                        required minLength={2}/>
                    <ValidationMessage msg={this.state.errores.apellido2} />
                    </div>
                </div>
                    <br />
                    <div className="form-group">
                    <label htmlFor='correo'>Correo Electrónico:</label>
                    <input type='email' className='form-control' id='correo' name='correo' value={this.state.elemento.correo} onChange={this.handleChange}
                        required minLength={3} />
                    <ValidationMessage msg={this.state.errores.correo} />
                    </div>
                    <br />
                <div className="row">
                    <div className="col">
                    <label htmlFor='dni'>DNI:</label>
                    <input type='text' className='form-control' id='nif' name='nif' value={this.state.elemento.nif} onChange={this.handleChange}
                        required />
                    <ValidationMessage msg={this.state.errores.nif} />
                    </div>
                    <br />
                    <div className="col">
                    <label htmlFor='text'>Teléfono:</label>
                    <input type='text' className='form-control' id='telefono' name='telefono' value={this.state.elemento.telefono} onChange={this.handleChange}
                        required />
                    <ValidationMessage msg={this.state.errores.telefono} />
                    </div>
                    <br />
                    <div className="col">
                    <label htmlFor='text'>País:</label>
                    <input type='text' className='form-control' id='pais' name='pais' value={this.state.elemento.pais} onChange={this.handleChange}
                        required />
                    <ValidationMessage msg={this.state.errores.pais} />
                    </div>
                    <br />
                    <div className="col">
                    <label htmlFor='text'>Ciudad:</label>
                    <input type='text' className='form-control' id='ciudad' name='ciudad' value={this.state.elemento.ciudad} onChange={this.handleChange}
                        required />
                    <ValidationMessage msg={this.state.errores.ciudad} />
                    </div>
                </div>
                    <br />
                <div className="row">
                    <div className="col">
                    <label htmlFor='text'>Calle:</label>
                    <input type='text' className='form-control' id='direccion' name='direccion' value={this.state.elemento.direccion} onChange={this.handleChange}
                        required minLength={2}/>
                    <ValidationMessage msg={this.state.errores.direccion} />
                    </div>
                    <br />
                    <div className="col">
                    <label htmlFor='text'>Código Postal:</label>
                    <input type='text' className='form-control' id='cp' name='cp' value={this.state.elemento.cp} onChange={this.handleChange}
                     required maxLength={5}/>
                    <ValidationMessage msg={this.state.errores.cp} />
                    </div>
                </div>
                    <br />
                <div className="form-group">
                    <label htmlFor='text'>IBAN:</label>
                    <input type='text' className='form-control' id='iban' name='iban' value={this.state.elemento.iban} onChange={this.handleChange}
                        required />
                    <ValidationMessage msg={this.state.errores.iban} />
                </div>
                <br />
                <div className="form-group">
                    <label htmlFor='text'>Experiencia: 
                    <select className="form-control" name="experiencia" value={this.state.elemento.experiencia} onChange={this.handleChange} required>
                    <optgroup label="opciones"/>
                    <option value="1">Principiante</option>
                    <option value="2">Intermedio</option>
                    <option value="3">Experto</option>
                    </select>
                    </label>
                </div>
                    <br />
                <div className="form-group">
                    <label htmlFor='text'>
                    Profesión: 
                    <select className="form-control" name="profesion" placeholder='Opciones' value={this.state.elemento.profesion} onChange={this.handleChange} required>
                    <optgroup label="opciones"/>
                    <option value="Grupo 1">Ingeniero, licenciado y alta direccion</option>
                    <option value="Grupo 2">Ingeniero técnico, perito y ayudantes con titulación</option>
                    <option value="Grupo 3">Jefatura administrativa</option>
                    <option value="Grupo 4">Personal administrativo</option>
                    <option value="Grupo 5">Auxiliar administrativo</option>
                    <option value="Grupo 6">Oficiales de primera y segunda</option>
                    <option value="Grupo 7">Oficiales de tercera y especialistas</option>
                    <option value="Grupo 8">Peones</option>
                    <option value="Grupo 9">Menores de 18 años</option>
                    </select>
                    </label>
                </div>
                < br/>
                <div className="seccionFormulario">Datos de registro del Inversor</div>
                    <div className="col">
                    <label htmlFor='text'>Usuario:</label>
                    <input type='text' placeholder='Introduce un nombre de usuario' className='form-control' id='username' name='username' value={this.state.elemento.username} onChange={this.handleChange}
                        required minLength={2}/>
                    <ValidationMessage msg={this.state.errores.username} />
                    </div>
                    < br/>
                
                    <div className="col">
                    <label htmlFor='text'>Contraseña:</label>
                    <input type='password' placeholder='Introduce una contraseña' className='form-control' id='password' name='password' value={this.state.elemento.password} onChange={this.handleChange}
                        required minLength={2}/>
                    <ValidationMessage msg={this.state.errores.password} />
                    <div>
                    <small id="passwordHelpBlock" className="form-text text-muted">
                    Tu contraseña debe tener un mínimo de 8 caracteres, con al menos una letra, una mayúscula, un número y un carácter especial.
                    </small>
                    </div>
                    </div>
                </div>
                <p className="container-fluid">
                <Link to ="/" type="button" className="btn btn-primary" disabled={this.state.esInvalido} onClick={() => { 
                        let elemento = this.state.elemento;

                        elemento.estado = 'PENDIENTE';
                        delete elemento.recontraseña
                        alert(JSON.stringify(elemento))
                        axios.post(
                            "http://localhost:4200/inversores/v1", {newUser:elemento}
                        )
                        }} >
                        Registrarse
                        </Link>
                    <Link type="button" className="btn btn-secondary" to="/" > Atrás</Link>
                </p>
                
                </form>
                </>
        )
    }
}
