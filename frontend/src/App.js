import React from "react";
import "./App.css";
import Inicio from "./inicio";
import Proyectos from "./ProyectosPromotor";
import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import "./App.css";
import Promotores, { PromotoresView } from "./Promotores";
import Inversores, { InversoresView } from "./Inversores";
import InversoresInv, { InversoresInvView } from "./Inversores";
import InversoresGest, { InversoresGestView } from "./InversoresGest";
import PromotoresProm, { PromotoresPromView } from "./Promotores";
import PromotoresGest, { PromotoresGestView } from "./PromotoresGest";
import FormularioPromotores from "./FormularioInscripciónProm";
import FormularioInversores from "./FormularioInscripciónInv";
import FormularioLogin from "./login";
import AmortizacionForm from "./PréstamoDef/AmortizacionForm";
import { LoginComponent, LogOutComponent } from "./security";
import { useSelector } from "react-redux";
import { selectIsInRole } from "./store/auth-slice";
import ProyectosPromotor, { VerProyectosProm } from "./ProyectosPromotor";
import ProyectosGestor, { VerProyectosGes } from "./ProyectosGestor";
import ProyectosInversor, { VerProyectosInv } from "./ProyectosInversor";
import MenuGestor from "./menugestor";
import MenuInversor from "./menuinversor";
import MenuPromotor from "./menupromotor";
import MenuUsuarioNormal from "./menuusuarionormal";
import Baja from "./PáginaÉxitoBaja";

export default function App() {
  const isGestor = useSelector(selectIsInRole("1"));
  const isPromotor = useSelector(selectIsInRole("2"));
  const isInversor = useSelector(selectIsInRole("3"));
  const resto = !isGestor && !isPromotor && !isInversor;

  return (
    <>
      <div className="todo">
        <BrowserRouter>
          <div className="arriba">
            <h1 className="titulo">Aplicación de crowdlending</h1>
            <nav className="navbar navbar-light bg-light">
              <Link className="navbar-brand" to="/">
                Inicio
              </Link>

              {isGestor && <MenuGestor />}
              {isPromotor && <MenuPromotor />}
              {isInversor && <MenuInversor />}
              {resto && <MenuUsuarioNormal />}
              <LogOutComponent />
            </nav>
          </div>

          <div className="dentro">
            <Routes>
              <Route path="inicio" element={<Inicio />}></Route>
              <Route path="Login" element={<LoginComponent />}></Route>
              {isPromotor && isPromotor && (
                <Route
                  path="promotores"
                  element={<FormularioPromotores />}
                ></Route>
              )}
              {isInversor && isGestor && (
                <Route
                  path="inversores"
                  element={<FormularioInversores />}
                ></Route>
              )}
              {isInversor && (
                <Route path="proyectos" element={<ProyectosInversor />}></Route>
              )}
              {isPromotor && (
                <Route path="proyectos" element={<ProyectosPromotor />}></Route>
              )}
              {isGestor && (
                <Route path="proyectos" element={<ProyectosGestor />}></Route>
              )}

              <Route
                path="promotores"
                element={<FormularioPromotores />}
              ></Route>
              <Route
                path="inversores"
                element={<FormularioInversores />}
              ></Route>
              <Route path="proyectos" element={<Proyectos />}></Route>

              {isPromotor && true && (
                <Route path="/bdpromotoresProm">
                  <Route index element={<PromotoresProm />} />
                  <Route path="add" element={<PromotoresProm />} />
                  <Route
                    path=":correo"
                    element={<PromotoresPromView />}
                  ></Route>
                  <Route path=":correo/edit" element={<PromotoresProm />} />
                </Route>
              )}
              {isGestor && true && (
                <Route path="/bdpromotoresGest">
                  <Route index element={<PromotoresGest />} />
                  {isGestor && (
                    <Route path="add" element={<PromotoresGest />} />
                  )}
                  <Route
                    path=":correo"
                    element={<PromotoresGestView />}
                  ></Route>
                  {isGestor && (
                    <Route path=":correo/edit" element={<PromotoresGest />} />
                  )}
                </Route>
              )}

              {isGestor && true && (
                <Route path="/bdinversoresGest">
                  <Route index element={<InversoresGest />} />
                  
                    <Route path="add" element={<InversoresGest />} />
                  
                  <Route
                    path=":correo"
                    element={<InversoresGestView />}
                  ></Route>
                  
                    <Route path=":correo/edit" element={<InversoresGest />} />
                  
                </Route>
              )}

              {isInversor && true && (
                <Route path="/bdinversoresInv">
                  <Route index element={<InversoresInv />} />
                  {isGestor && (
                    <Route path="add" element={<InversoresInv />} />
                  )}
                  <Route
                    path=":correo"
                    element={<InversoresInvView />}
                  ></Route>
                  {isGestor && (
                    <Route path=":correo/edit" element={<InversoresInv />} />
                  )}
                </Route>
              )}
              {true && (
                <Route path="/ProyectosProm">
                  <Route index element={<Proyectos />} />
                  <Route path="add" element={<Proyectos />} />
                  <Route path=":id" element={<VerProyectosProm />} />
                  <Route path=":id/edit" element={<Proyectos />} />
                </Route>
              )}
              {true && (
                <Route path="/ProyectosInv">
                  <Route index element={<Proyectos />} />
                  <Route path=":id" element={<VerProyectosInv />} />
                </Route>
              )}
              {true && (
                <Route path="/ProyectosGes">
                  <Route index element={<Proyectos />} />
                  <Route path=":id" element={<VerProyectosGes />} />
                  <Route path=":id/edit" element={<Proyectos />} />
                </Route>
              )}
              <Route path="simulador" element={<AmortizacionForm />}></Route>
              <Route path="*" element={<Inicio />}></Route>
              {resto && (<Route path="exbaja" element={<Baja />}></Route>)}
            </Routes>
          </div>
        </BrowserRouter>
        <div className="pie">
          <div>&copy; 2022 Aplicación de Crowdlending S.L</div>
          <div>Frase diciendo algo de la CNMV</div>
        </div>
      </div>
    </>
  );
}
