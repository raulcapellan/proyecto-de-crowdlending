import { Link } from "react-router-dom";
import "./inicio.css";
function MenuGestor(props) {
  return (
    <>
        <Link className="navbar-brand" to="/bdpromotoresGest">
          Área promotores
        </Link>
        <Link className="navbar-brand" to="/bdinversoresGest">
          Área inversores
        </Link>
        <Link className="navbar-brand" to="/proyectos">
          Proyectos
        </Link>
        <Link className="navbar-brand" to="/simulador">
          Simulador de Préstamos
        </Link>
     </>
  );
}

export default MenuGestor;
