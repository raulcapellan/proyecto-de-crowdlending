import React, { Component, useState, useEffect } from "react";
import axios from "axios";
import { ValidationMessage, Esperando } from "./comunes";
import { Outlet, useLocation, useParams } from 'react-router-dom';
import store from "./store/store";
import { esNIF } from './DNI'
import { fnValidateIBAN } from './IBAN.js'
import { Link } from "react-router-dom";
import MenuGestor from "./menugestor";






const withRouter = WrappedComponent => props => {
  const params = useParams();
  const { pathname } = useLocation()
  const match = { url: pathname, params: useParams() }

  return (
    <WrappedComponent
      {...props}
      params={params}
      match={match}
    />
  );
};
class PromotoresGest extends Component {
   
  constructor(props) {
    
    super(props);
    let pagina = props?.match?.params?.page ? props.match.params.page : 0;
    this.state = {
      modo: "list",
      listado: [],
      elemento: null,
      loading: true,
      pagina
    };
    this.correoOriginal = null;
    this.url = "http://localhost:4200/promotGest/v1"; //Versionar
  }

  list() {
    this.setState({ loading: true });
    axios
      .get(`${this.url}`)
      .then(resp => {
        let paginas = resp.pages;
        let pagina = this.state.pagina < paginas ? this.state.pagina : paginas - 1;
        this.setState({ pagina, paginas });
        axios
          .get(`${this.url}`)
          .then(resp => {
            this.setState({
              modo: "list",
              listado: resp.data,
              loading: false
            });
          })
          .catch(err => {
            store.AddErrNotify(err);
            this.setState({ loading: false });
          });
      }).catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });

  }
  add() {
    this.setState({
      modo: "add",
      elemento: { correo:"", rating: "", estado: ""}
    });
  }
  edit(key) {
    this.setState({ loading: true });
    axios
      .get(this.url + `/${key}`)
      .then(resp => {
        this.setState({
          modo: "edit",
          elemento: resp.data,
          loading: false
        });
        this.correoOriginal = key;
      })
      .catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }
  view(key) {
    this.setState({ loading: true });
    axios
      .get(this.url + `/${key}`)
      .then(resp => {
        this.setState({
          modo: "view",
          elemento: resp.data,
          loading: false
        });
      })
      .catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }
  delete(key) {
    if (!window.confirm("¿Seguro?")) return;
    this.setState({ loading: true });
    axios
      .delete(this.url + `/${key}`)
      .then(() => this.list())
      .catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }
  cancel() {
    this.list();
  }
  send(elemento) {
    // eslint-disable-next-line default-case
    switch (this.state.modo) {
      case "add":
        axios
          .post(this.url, elemento)
          .then(data => this.cancel())
          .catch(err => {
            store.AddErrNotify(err);
            this.setState({ loading: false });
          });
        break;
      case "edit":
        axios
          .put(this.url + `/${this.correoOriginal}`, elemento)
          .then(data => this.cancel())
          .catch(err => {
            store.AddErrNotify(err);
            this.setState({ loading: false });
          });
        break;
    }
  }
  decodeRuta() {
    if (this.props.match.url === this.urlActual) return;
    this.urlActual = this.props.match.url;
    if (this.props.match.params.correo) {
      if (this.props.match.url.endsWith('/edit'))
        this.edit(this.props.match.params.correo);
      else
        this.view(this.props.match.params.correo);
    } else {
      if (this.props.match.url.endsWith('/add'))
        this.add();
      else
        this.list();
    }
  }

  componentDidMount() {
    this.decodeRuta();
  }
  componentDidUpdate() {
    this.decodeRuta();
  }

  render() {
    if (this.state.loading) return <Esperando />;
    switch (this.state.modo) {
      case "add":
        return (
          <PromotoresForm
            isAdd={this.state.modo === "add"}
            elemento={this.state.elemento}
            onCancel={e => this.cancel()}
            onSend={e => this.send(e)}
          />
        );
      case "edit":
        return (
          <PromotoresForm
            isAdd={this.state.modo === "add"}
            elemento={this.state.elemento}
            onCancel={e => this.cancel()}
            onSend={e => this.send(e)}
          />
        );
      case "view":
        return (
          <PromotoresGestView
            detalle={this.state.detalle}
            isView={this.state.modo === "view"}
            elemento={this.state.elemento}
            onCancel={() => this.cancel()}
          />
        );
      default:
        return (
          <PromotoresList
            listado={this.state.listado}
            onAdd={e => this.add()}
            onView={key => this.view(key)}
            onEdit={key => this.edit(key)}
            onDelete={key => this.delete(key)}
          />
        );
    }
  }
}

export class PromotoresList extends Component {
  
  render() {
    return (
      
      <table className="table table-striped">
        <thead className="thead-dark">
          <tr>
            <th>Nombre</th>
            <th>Rating</th>
            <th>Estado</th>
            <th>
              <Link type="button" className="btn btn-danger" to="/" > Volver</Link>
            </th>
          </tr>
        </thead>
        <tbody>
          {this.props.listado.map(item => (
            <tr key={item.correo}>
              <td>
                {item["nombre"]} {item.apellido1} {item.apellido2}
              </td>
              <td>
                {item["rating"]}
              </td>
              <td>
                {item["estado"]}
              </td>
              <td>
                <Link to={"/bdpromotoresGest/" + item.correo} type="button" className="btn btn-primary">
                  Ver
                </Link>
                <input
                  type="button"
                  className="btn btn-warning"
                  value="Añadir Rating"
                  onClick={e => this.props.onEdit(item.correo)}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}
export function PromotoresGestView() {
  const [elemento, setElemento] = useState({});
  const url = "http://localhost:4200/promotGest/v1";
  const { correo } = useParams();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    axios
      .get(url + `/${correo}`)
      .then(resp => {
        setElemento(resp.data)
        setLoading(false);
      })
      .catch(err => {
        store.AddErrNotify(err);
        setLoading(false);
      });
  }, [correo]);
  if (loading) return <Esperando />;
  return (
    <form>
      <div>
        <b>Nombre:</b> {elemento.nombre}
        <br />
        <b>Primer Apellido:</b> {elemento.apellido1}
        <br />
        <b>Segundo Apellido:</b> {elemento.apellido2}
        <br />
        <b>email:</b> {elemento.correo}
        <br />
        <b>DNI:</b> {elemento.nif}
        <br />
        <b>Teléfono:</b> {elemento.telefono}
        <br />
        <b>Ciudad:</b> {elemento.ciudad}
        <br />
        <b>País:</b> {elemento.pais}
        <br />
        <b>Calle:</b> {elemento.direccion}
        <br />
        <b>Código Postal:</b> {elemento.cp}
        <br />
        <b>IBAN:</b> {elemento.iban}
        <br />
        <b>Profesión:</b> {elemento.profesion}
        <br />
        <b>Estado:</b> {elemento.estado}
        <br />
        <b>Scoring del perfil:</b> {elemento.rating}
        <br />
      </div>
      <div>
        <p>
          <Link type="button" className="btn btn-primary" to="/bdpromotoresGest" > Volver</Link>
        </p>
      </div>
    </form>
  );
}


export class PromotoresForm extends Component {
  constructor(props) {
    super(props);
    this.state = { elemento: props.elemento, msgErr: [], invalid: false };
    this.handleChange = this.handleChange.bind(this);
    this.onSend = () => {
      if (this.props.onSend) this.props.onSend(this.state.elemento);
    };
    this.onCancel = () => {
      if (this.props.onCancel) this.props.onCancel();
    };
  }
  handleChange(event) {
    const cmp = event.target.name;
    const valor = event.target.value;
    this.setState(prev => {
      prev.elemento[cmp] = valor;
      return { elemento: prev.elemento };
    });
    this.validar();
  }
  validarCntr(cntr) {
    if (cntr.name) {
      // eslint-disable-next-line default-case
      switch (cntr.name) {
        case "nombre":
          cntr.setCustomValidity(cntr.value !== cntr.value.toUpperCase()
            ? "Debe estar en mayúsculas" : '');
          break;
      }
    }
  }
  validar() {
    if (this.form) {
      const errors = {};
      let invalid = false;
      for (var cntr of this.form.elements) {
        if (cntr.name) {
          this.validarCntr(cntr);
          errors[cntr.name] = cntr.validationMessage;
          invalid = invalid || !cntr.validity.valid;
        }
      }
      this.setState({ msgErr: errors, invalid: invalid });
    }
  }
  componentDidMount() {
    this.validar();
  }
  validate() {
    if (this.form) {
      let esInvalido = false
      let errores = {}
      for (let cntr of this.form.current.elements) {
        if (cntr.name) {
          // eslint-disable-next-line default-case
          switch (cntr.name) {
            case 'nif':
              if (cntr.value && !esNIF(cntr.value))
                cntr.setCustomValidity('No es un NIF valido')
              else
                cntr.setCustomValidity('')
              break;

            case 'IBAN':
              if (cntr.value && !fnValidateIBAN(cntr.value))
                cntr.setCustomValidity('No es un IBAN válido')
              else
                cntr.setCustomValidity('')
              break;

          }
          errores[cntr.name] = cntr.validationMessage;
          if (!cntr.validity.valid) esInvalido = true
        }
      }
      this.setState({ errores, esInvalido });
    }
  }
  render() {
    return (
      <form
        ref={tag => {
          this.form = tag;
        }}
      >
    {this.props.isAdd && (
          <div className="form-group">
            <label htmlFor="correo">Correo</label>
            <input
              type="number"
              className="form-control"
              id="correo"
              name="correo"
              value={this.state.elemento.correo}
              onChange={this.handleChange}
              required
            />
            <ValidationMessage msg={this.state.msgErr.correo} />
          </div>
        )}
     
    <div className="container-fluid">
    {!this.props.isAdd && (
          <div className="form-group">
            <label></label>
            {this.state.elemento.correo}
          </div>
        )}
        <div className="form-group">
          <label htmlFor="rating">Calificación Crediticia:
          <select type="text" className="form-control" id="rating" name="rating" placeholder='Opciones' value={this.state.elemento.rating} onChange={this.handleChange} required>
                    <optgroup label="opciones"/>
                    <option value="AAA">AAA</option>
                    <option value="AA">AA</option>
                    <option value="A">A</option>
                    <option value="BBB">BBB</option>
                    <option value="BB">BB</option>
                    <option value="B">B</option>
                    <option value="CCC">CCC</option>
                    <option value="CC">CC</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
            </select>
          </label>
          <ValidationMessage msg={this.state.msgErr.rating} />
        </div><div className="form-group">
          <label htmlFor="estado">Estado
          <select type="text" className="form-control" id="estado" name="estado" placeholder='Opciones' value={this.state.elemento.estado} onChange={this.handleChange} required>
                    <optgroup label="opciones"/>
                    <option value="Pendiente">Pendiente</option>
                    <option value="Aprobado">Aprobado</option>
                    <option value="Rechazado">Rechazado</option>
            </select>
            </label>
          <ValidationMessage msg={this.state.msgErr.estado} />
        </div>

        <div className="form-group">
          <button
            className="btn btn-primary"
            type="button"
            disabled={this.state.invalid}
            onClick={this.onSend}
          >
            Enviar
          </button>
          <button
            className="btn btn-primary"
            type="button"
            onClick={this.onCancel}
          >
            Volver
          </button>
        </div>
        </div>
      </form>
    );
  }
}

export default withRouter(PromotoresGest)
