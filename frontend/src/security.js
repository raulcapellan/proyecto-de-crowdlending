import React, { useState } from "react";
import { useDispatch } from 'react-redux'
import axios from "axios";
import { login, logout } from './store/auth-slice'
import { Esperando } from './comunes';
import store from "./store/store";
import { ValidationMessageLogin } from './comunes';
import { Link, Navigate, useNavigate } from "react-router-dom";
import Principal from "./inicio"


const API_AUTH = process.env.REACT_APP_API_AUTH || 'http://localhost:4200/api/'

axios.interceptors.request.use((config) => {
    if (store.getState().auth.isAuth)
        config.headers['Authorization'] = store.getState().auth.authToken;
    // XSRF
    // config.withCredentials = true;
    return config;
}, (error) => {
    return Promise.reject(error);
});

export default axios;

export const LoginComponent = () => {
    const [auth, setAuth] = useState(store.getState().auth);
    const [loading, setLoading] = useState(false);
    const [usr, setUser] = useState('admin')
    const [pwd, setPwd] = useState('P@$$w0rd')
    const dispatch = useDispatch();

    const onLogin = () => {
        setLoading(true);
        axios.post(API_AUTH + 'login', { name: usr, password: pwd })
            .then(resp => {
                if (resp.data.success) {
                    dispatch(login({ authToken: resp.data.token, name: resp.data.name, roles: resp.data.roles }))
                    setAuth(store.getState().auth)
                    
                } else {
                    store.AddErrNotify('Usuario o contraseña invalida.')
                }
                setLoading(false);
            }
            ).catch(err => {
                store.AddErrNotify(err);
                setLoading(false);
            });
    }
    const onLogout = () => {
        dispatch(logout())
        setAuth(store.getState().auth)
    }

    if (loading) return <Esperando />;

    if (auth.isAuth)
   return  <Navigate to="/" replace={true} />
               
    return (
        
           <div className="bg">
               <form className="container-fluid row vh-100">
                   <div className="container">
                   <h1 className='display-3 text-center text-light'>
                   <i className="bi bi-cash-coin"  ></i>
                   </h1>
                   <div className="col">
                   <input type='text' placeholder='Introduce tu usuario' className= 'form-control' id='usuario' name='usuario' value={usr} onChange={(e) => setUser(e.target.value)}
                       required />
                   </div>
                   <br />
                   <div className="col">
                   <input type='password'placeholder='Introduce tu contraseña' className= 'form-control' id='contraseña' name='contraseña' value={pwd} onChange={(e) => setPwd(e.target.value)}
                       required/>
                   </div>
                   <br />
                   <input type="button" className="btn btn-light w-100" value="ACCEDER" onClick={onLogin}/>
                   
                   </div>
                   <p className="container-fluid">
                    <Link type="button" className="btn btn-secondary" to="/" > Volver</Link>
                    </p> 
               </form>
               </div>
    );
}

export const LogOutComponent = () => {
    const [auth, setAuth] = useState(store.getState().auth);
    const dispatch = useDispatch();
    let navigate = useNavigate();

    store.subscribe(()=> {
        setAuth(store.getState().auth)
    })

   const onLogout = () => {
        dispatch(logout())
        setAuth(store.getState().auth)
        navigate("/", { replace: true });
    }

    if (auth.isAuth)
    return <div>Hola {auth.name} <input className="btn btn-outline-success btn-sm" type="button" value="logout" onClick={onLogout} /></div>
               
    return null;
}