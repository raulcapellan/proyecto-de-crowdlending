import React, { Component, useState, useEffect } from "react";
import axios from "axios";
import { ValidationMessage, Esperando } from "./comunes";
import { Outlet, useLocation, useParams } from 'react-router-dom';
import store from "./store/store";
import { esNIF } from './DNI'
import { fnValidateIBAN } from './IBAN.js'
import { Link } from "react-router-dom";
import MenuPromotor from "./menupromotor";





const withRouter = WrappedComponent => props => {
  const params = useParams();
  const { pathname } = useLocation()
  const match = { url: pathname, params: useParams() }

  return (
    <WrappedComponent
      {...props}
      params={params}
      match={match}
    />
  );
};
class PromotoresProm extends Component {
  constructor(props) {
    super(props);
    let pagina = props?.match?.params?.page ? props.match.params.page : 0;
    this.state = {
      modo: "list",
      listado: [],
      elemento: null,
      loading: true,
      pagina
    };
    this.correoOriginal = null;
    this.url = "http://localhost:4200/promot/v1";//Versionar
  }

  list() {
    this.setState({ loading: true });
    axios
      .get(`${this.url}`)
      .then(resp => {
        let paginas = resp.pages;
        let pagina = this.state.pagina < paginas ? this.state.pagina : paginas - 1;
        this.setState({ pagina, paginas });
        axios
          .get(`${this.url}`)
          .then(resp => {
            this.setState({
              modo: "list",
              listado: resp.data,
              loading: false
            });
          })
          .catch(err => {
            store.AddErrNotify(err);
            this.setState({ loading: false });
          });
      }).catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });

  }
  add() {
    this.setState({
      modo: "add",
      elemento: { correo: "", nombre: "", apellido1: "", apellido2: "", nif: "", telefono: "", ciudad: "", pais: "", direccion: "", cp: "", iban: "", profesion: "" }
    });
  }
  edit(key) {
    this.setState({ loading: true });
    axios
      .get(this.url + `/${key}`)
      .then(resp => {
        this.setState({
          modo: "edit",
          elemento: resp.data,
          loading: false
        });
        this.correoOriginal = key;
      })
      .catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }
  view(key) {
    this.setState({ loading: true });
    axios
      .get(this.url + `/${key}`)
      .then(resp => {
        this.setState({
          modo: "view",
          elemento: resp.data,
          loading: false
        });
      })
      .catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }
  delete(key) {
    if (!window.confirm("¿Seguro?")) return;
    this.setState({ loading: true });
    axios
      .delete(this.url + `/${key}`)
      .then(() => this.list())
      .catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }
  cancel() {
    this.list();
  }
  send(elemento) {
    // eslint-disable-next-line default-case
    switch (this.state.modo) {
      case "add":
        axios
          .post(this.url, elemento)
          .then(data => this.cancel())
          .catch(err => {
            store.AddErrNotify(err);
            this.setState({ loading: false });
          });
        break;
      case "edit":
        axios
          .put(this.url + `/${this.correoOriginal}`, elemento)
          .then(data => this.cancel())
          .catch(err => {
            store.AddErrNotify(err);
            this.setState({ loading: false });
          });
        break;
    }
  }
  decodeRuta() {
    if (this.props.match.url === this.urlActual) return;
    this.urlActual = this.props.match.url;
    if (this.props.match.params.correo) {
      if (this.props.match.url.endsWith('/edit'))
        this.edit(this.props.match.params.correo);
      else
        this.view(this.props.match.params.correo);
    } else {
      if (this.props.match.url.endsWith('/add'))
        this.add();
      else
        this.list();
    }
  }

  componentDidMount() {
    this.decodeRuta();
  }
  componentDidUpdate() {
    this.decodeRuta();
  }

  render() {
    if (this.state.loading) return <Esperando />;
    switch (this.state.modo) {
      case "add":
        return (
          <PromotoresForm
            isAdd={this.state.modo === "add"}
            elemento={this.state.elemento}
            onCancel={e => this.cancel()}
            onSend={e => this.send(e)}
          />
        );
      case "edit":
        return (
          <PromotoresForm
            isAdd={this.state.modo === "add"}
            elemento={this.state.elemento}
            onCancel={e => this.cancel()}
            onSend={e => this.send(e)}
          />
        );
      case "view":
        return (
          <PromotoresPromView
            detalle={this.state.detalle}
            isView={this.state.modo === "view"}
            elemento={this.state.elemento}
            onCancel={() => this.cancel()}
          />
        );
      default:
        return (
          <PromotoresList
            listado={this.state.listado}
            onAdd={e => this.add()}
            onView={key => this.view(key)}
            onEdit={key => this.edit(key)}
            onDelete={key => this.delete(key)}
          />
        );
    }
  }
}
export class PromotoresList extends Component {
  render() {
    return (
      <>
        <table className="table table-striped">
          <thead className="thead-dark">
            <tr>
              <th>Nombre</th>
              <th>Estado</th>
              <th>Rating</th>
              <th>
                <Link type="button" className="btn btn-danger" to="/" > Volver</Link>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.props.listado.map(item => (
              <tr key={item.correo}>
                <td>
                  {item["nombre"]} {item.apellido1} {item.apellido2}
                </td>
                <td>
                  {item["estado"]}
                </td>
                <td>
                  {item["rating"]}
                </td>
                <td>
                  <Link to={"/bdpromotoresProm/" + item.correo} type="button" className="btn btn-primary">
                    Ver
                  </Link>
                  <input
                    type="button"
                    className="btn btn-success"
                    value="Editar"
                    onClick={e => this.props.onEdit(item.correo)}
                  />
                  <Link to={"/exbaja/"}
                    type="button"
                    className="btn btn-danger"
                    onClick={e => this.props.onDelete(item.correo)}
                  >
                  Darse de baja
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </>
    );
  }
}
export function PromotoresPromView() {
  const [elemento, setElemento] = useState({});
  const url = "http://localhost:4200/promot/v1";
  const { correo } = useParams();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    axios
      .get(url + `/${correo}`)
      .then(resp => {
        setElemento(resp.data)
        setLoading(false);
      })
      .catch(err => {
        store.AddErrNotify(err);
        setLoading(false);
      });
  }, [correo]);
  if (loading) return <Esperando />;
  return (
    <>
      <form>
        <div>
          <b>Nombre:</b> {elemento.nombre}
          <br />
          <b>Primer Apellido:</b> {elemento.apellido1}
          <br />
          <b>Segundo Apellido:</b> {elemento.apellido2}
          <br />
          <b>email:</b> {elemento.correo}
          <br />
          <b>DNI:</b> {elemento.nif}
          <br />
          <b>Teléfono:</b> {elemento.telefono}
          <br />
          <b>Ciudad:</b> {elemento.ciudad}
          <br />
          <b>País:</b> {elemento.pais}
          <br />
          <b>Calle:</b> {elemento.direccion}
          <br />
          <b>Código Postal:</b> {elemento.cp}
          <br />
          <b>IBAN:</b> {elemento.iban}
          <br />
          <b>Profesión:</b> {elemento.profesion}
          <br />
          <b>Estado:</b> {elemento.estado}
          <br />
          <b>Scoring del perfil:</b> {elemento.rating}
          <br />
          <b>Fecha de creación:</b> {elemento.creationDate}
          <br />
          <b>Fecha de modificación:</b> {elemento.modificationDate}
          <br />

        </div>
        <div>
          <p>
            <Link type="button" className="btn btn-primary" to="/bdpromotoresProm" > Volver</Link>
          </p>
        </div>
      </form>
    </>
  );
}


export class PromotoresForm extends Component {
  constructor(props) {
    super(props);
    this.state = { elemento: props.elemento, msgErr: [], invalid: false };
    this.handleChange = this.handleChange.bind(this);
    this.onSend = () => {
      if (this.props.onSend) this.props.onSend(this.state.elemento);
    };
    this.onCancel = () => {
      if (this.props.onCancel) this.props.onCancel();
    };
  }
  handleChange(event) {
    const cmp = event.target.name;
    const valor = event.target.value;
    this.setState(prev => {
      prev.elemento[cmp] = valor;
      return { elemento: prev.elemento };
    });
    this.validar();
  }
  validarCntr(cntr) {
    if (cntr.name) {
      // eslint-disable-next-line default-case
      switch (cntr.name) {
        case 'nif':
              if (cntr.value && !esNIF(cntr.value))
                cntr.setCustomValidity('No es un NIF valido')
              else
                cntr.setCustomValidity('')
              break;

            case 'iban':
              if (cntr.value && !fnValidateIBAN(cntr.value))
                cntr.setCustomValidity('No es un IBAN válido')
              else
                cntr.setCustomValidity('')
              break;
      }
    }
  }
  validar() {
    if (this.form) {
      const errors = {};
      let invalid = false;
      for (var cntr of this.form.elements) {
        if (cntr.name) {
          this.validarCntr(cntr);
          errors[cntr.name] = cntr.validationMessage;
          invalid = invalid || !cntr.validity.valid;
        }
      }
      this.setState({ msgErr: errors, invalid: invalid });
    }
  }
  componentDidMount() {
    this.validar();
  }
  
  render() {
    return (
      <>
        <form
          ref={tag => {
            this.form = tag;
          }}
        >
          {this.props.isAdd && (
            <div className="form-group">
              <label htmlFor="correo">Correo</label>
              <input
                type="number"
                className="form-control"
                id="correo"
                name="correo"
                value={this.state.elemento.correo}
                onChange={this.handleChange}
                required
              />
              <ValidationMessage msg={this.state.msgErr.correo} />
            </div>
          )}
          {!this.props.isAdd && (
            <div className="form-group">
              <label></label>
              {this.state.elemento.correo}
            </div>
          )}
          <div className="form-group">
            <label htmlFor="nombre">Nombre</label>
            <input
              type="text"
              className="form-control"
              id="nombre"
              name="nombre"
              value={this.state.elemento.nombre}
              onChange={this.handleChange}
              required
              minLength={2}
            />
            <ValidationMessage msg={this.state.msgErr.nombre} />
          </div>
          <div className="form-group">
            <label htmlFor="apellido1">Primer Apellido</label>
            <input
              type="text"
              className="form-control"
              id="apellido1"
              name="apellido1"
              value={this.state.elemento.apellido1}
              onChange={this.handleChange}
              required
              minLength="2"
            />
            <ValidationMessage msg={this.state.msgErr.apellido1} />
          </div>
          <div className="form-group">
            <label htmlFor="apellido2">Segundo Apellido</label>
            <input
              type="text"
              className="form-control"
              id="apellido2"
              name="apellido2"
              value={this.state.elemento.apellido2}
              onChange={this.handleChange}
              required
              minLength={2}
            />
            <ValidationMessage msg={this.state.msgErr.apellido2} />
          </div>
          <div className="form-group">
            <label htmlFor="correo">Email</label>
            <input
              type="email"
              className="form-control"
              id="correo"
              name="correo"
              value={this.state.elemento.correo}
              onChange={this.handleChange}
              required
              minLength={3}
            />
            <ValidationMessage msg={this.state.msgErr.correo} />
          </div><div className="form-group">
            <label htmlFor="nif">DNI</label>
            <input
              type="text"
              className="form-control"
              id="nif"
              name="nif"
              value={this.state.elemento.nif}
              onChange={this.handleChange}
              required

            />
            <ValidationMessage msg={this.state.msgErr.nif} />
          </div><div className="form-group">
            <label htmlFor="telefono">Teléfono</label>
            <input
              type="text"
              className="form-control"
              id="telefono"
              name="telefono"
              value={this.state.elemento.telefono}
              onChange={this.handleChange}
              required
            />
            <ValidationMessage msg={this.state.msgErr.telefono} />
          </div><div className="form-group">
            <label htmlFor="ciudad">Ciudad</label>
            <input
              type="text"
              className="form-control"
              id="ciudad"
              name="ciudad"
              value={this.state.elemento.ciudad}
              onChange={this.handleChange}
              required
            />
            <ValidationMessage msg={this.state.msgErr.ciudad} />
          </div><div className="form-group">
            <label htmlFor="pais">País</label>
            <input
              type="text"
              className="form-control"
              id="pais"
              name="pais"
              value={this.state.elemento.pais}
              onChange={this.handleChange}
              required
            />
            <ValidationMessage msg={this.state.msgErr.pais} />
          </div><div className="form-group">
            <label htmlFor="direccion">Dirección</label>
            <input
              type="text"
              className="form-control"
              id="direccion"
              name="direccion"
              value={this.state.elemento.direccion}
              onChange={this.handleChange}
              required
            />
            <ValidationMessage msg={this.state.msgErr.direccion} />
          </div>
          <div className="form-group">
            <label htmlFor="cp">Código Postal</label>
            <input
              type="text"
              className="form-control"
              id="cp"
              name="cp"
              value={this.state.elemento.cp}
              onChange={this.handleChange}
              required
              maxLength={5}
              
            />
            <ValidationMessage msg={this.state.msgErr.cp} />
          </div><div className="form-group">
            <label htmlFor="iban">IBAN</label>
            <input
              type="text"
              className="form-control"
              id="iban"
              name="iban"
              value={this.state.elemento.iban}
              onChange={this.handleChange}
              required
            />
            <ValidationMessage msg={this.state.msgErr.iban} />
          </div><div className="form-group">
            <label htmlFor="profesion">Profesión</label>
            <input
              type="text"
              className="form-control"
              id="profesion"
              name="profesion"
              value={this.state.elemento.profesion}
              onChange={this.handleChange}
            />
            <ValidationMessage msg={this.state.msgErr.profesion} />
          </div>

          <div className="form-group">
            <button
              className="btn btn-primary"
              type="button"
              disabled={this.state.invalid}
              onClick={this.onSend}
            >
              Enviar
            </button>
            <button
              className="btn btn-primary"
              type="button"
              onClick={this.onCancel}
            >
              Volver
            </button>
          </div>
        </form>
      </>
    );
  }
}

export default withRouter(PromotoresProm)
