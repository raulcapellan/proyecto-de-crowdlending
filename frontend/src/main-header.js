import { Container, Nav, Navbar,  } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import logo from './logo.svg';

export default function MainHeader() {
  return (
    <header style={{background: "#f7df1e"}}>
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand><Link to="/"><img src={logo} className="App-logo" alt="logo" /></Link></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Item><NavLink className="nav-link" to="/DivProyectos">Datos Inversores</NavLink></Nav.Item>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  )
}
