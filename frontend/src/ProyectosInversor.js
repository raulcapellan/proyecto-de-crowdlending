import React, { Component, useState, useEffect } from "react";
import axios from "axios";
import { Esperando } from "./comunes";
import { useLocation, useParams } from 'react-router-dom';
import store from "./store/store";
import { Link } from "react-router-dom";

const withRouter = WrappedComponent => props => {
  const params = useParams();
  const { pathname } = useLocation()
  const match = { url: pathname, params: useParams() }

  return (
    <WrappedComponent
      {...props}
      params={params}
      match={match}
    />
  );
};

class ProyectosparaInv extends Component {
  constructor(props) {
    super(props);
    let pagina = props?.match?.params?.page ? props.match.params.page : 0;
    this.state = {
      modo: "list",
      listado: [],
      elemento: null,
      loading: true,
      pagina
    };
    this.idOriginal = null;
    this.url = "http://localhost:4200/inversor/proyectos/v1";
  }

  list() {
    this.setState({ loading: true });
    axios
      .get(`${this.url}`)
      .then(resp => {
        let paginas = resp.pages;
        let pagina = this.state.pagina < paginas ? this.state.pagina : paginas - 1;
        this.setState({ pagina, paginas });
        axios
          .get(`${this.url}`)
          .then(resp => {
            this.setState({
              modo: "list",
              listado: resp.data,
              loading: false
            });
          })
          .catch(err => {
            store.AddErrNotify(err);
            this.setState({ loading: false });
          });
      }).catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }

  view(key) {
    this.setState({ loading: true });
    axios
      .get(this.url + `/${key}`)
      .then(resp => {
        this.setState({
          modo: "view",
          elemento: resp.data,
          loading: false
        });
      })
      .catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }

  cancel() {
    this.list();
  }
  decodeRuta() {
    if (this.props.match.url === this.urlActual) return;
    this.urlActual = this.props.match.url;
    if (this.props.match.params.id) {
      if (this.props.match.url.endsWith('/edit'))
        this.edit(this.props.match.params.id);
      else
        this.view(this.props.match.params.id);
    } else {
      if (this.props.match.url.endsWith('/add'))
        this.add();
      else
        this.list();
    }
  }

  componentDidMount() {
    this.decodeRuta();
  }
  componentDidUpdate() {
    this.decodeRuta();
  }

  render() {
    if (this.state.loading) return <Esperando />;
    switch (this.state.modo) {
      case "view":
        return (
          <VerProyectosInv
            detalle={this.state.detalle}
            isView={this.state.modo === "view"}
            elemento={this.state.elemento}
            onCancel={() => this.cancel()}
          />
        );

      default:
        return (
          <ListaProyectos
            listado={this.state.listado}
            onAdd={e => this.add()}
            onView={key => this.view(key)}
            onEdit={key => this.edit(key)}
            onDelete={key => this.delete(key)}
          />
        );
    }
  }
}
export class ListaProyectos extends Component {
  render() {
    return (
      <>
        <table className="table table-striped table-hover">
          <thead className="table-info">
            <tr>
              <th >Nombre del proyecto</th>
              <th >Creador</th>
              <th>Estado</th>
              <th>Nominal (€)</th>
              <th></th>
             
            </tr>
          </thead>
          <tbody className="border border-light">
            {this.props.listado.map(item => (
              <tr key={item.id}>
                <td className="text-primary">
                  {item.nombre}
                </td>
                <td className="text-success ">
                  {item.creador}
                </td>
                <td className="text-danger">
                  {item.estado}
                </td>
                <td className="text-success ">
                  {item.nominal}
                </td>
                <td>
                  <Link to={"/ProyectosInv/" + item.id} type="button" className="btn btn-warning">
                    Ver detalle
                  </Link>

                  <br />
                </td>

              </tr>
            ))}
          </tbody>

        </table>

      </>
    );
  }
}

export function VerProyectosInv() {
  const [elemento, setElemento] = useState({});
  const url = "http://localhost:4200/inversor/proyectos/v1";
  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    axios
      .get(url + `/${id}`)
      .then(resp => {
        setElemento(resp.data)
        setLoading(false);
      })
      .catch(err => {
        store.AddErrNotify(err);
        setLoading(false);
      });
  }, [id]);

  function invertir() {
    const urlInvertir = "http://localhost:4200/inversor/proyectos/v1"
    axios
      .put(urlInvertir + `/${id}`, elemento)
      .then(resp => {
        setElemento(resp.data)
        setLoading(false);
      })
      .catch(err => {
        store.AddErrNotify(err);
        setLoading(false);
      });
  }

  if (loading) return <Esperando />;
  return (
    <form >
      <p className="text-center">
        <img src={elemento.imagen} className="rounded-circle" alt="Foto del proyecto" />
      </p>
      <p className="text-center" >
        <br />
        <b>ID:</b> {elemento.id}
        <br />
        <b>Nombre del proyecto:</b> {elemento.nombre}
        <br />
        <b>Descrición del proyecto: </b> {elemento.descripcion}
        <br />
        <b>Nominal del préstamo: </b> {elemento.nominal}
        <br />
        <b>Tipo de interes: </b> {elemento.interes}
        <br />
        <b>Tipo de proyecto: </b> {elemento.tipo}
        <br />
        <b>Plazo de amortizacion: </b> {elemento.plazos}
        <br />
        <b>Rating: </b> {elemento.rating}
        <br />
        <b>Estado: </b> {elemento.estado}
        <br />
        <b>Creador: </b> {elemento.creador}
        <br />
        <b>Fecha de creación: </b> {elemento.creationDate}
        <br />
        <b>Fecha de modificación: </b> {elemento.modificationDate}
        <br />
      </p>
      {elemento && elemento.estado && elemento.estado !== 'ADJUDICADO' && <input type="button" value={"Invertir"} className="btn btn-warning" onClick={invertir}></input>}
      <p>
        <Link type="button" className="btn btn-primary" to="/proyectos/" > Volver</Link>
      </p>

    </form>
  );
}

export default withRouter(ProyectosparaInv);
