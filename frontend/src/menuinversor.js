import { Link } from "react-router-dom";
import "./inicio.css";
function MenuInversor(props) {
  return (
    <>
        <Link className="navbar-brand" to="/bdinversoresInv">
          Área inversores
        </Link>
        <Link className="navbar-brand" to="/proyectos">
          Proyectos
        </Link>
        <Link className="navbar-brand" to="/simulador">
          Simulador de Préstamos
        </Link>
    </>
  );
}

export default MenuInversor;
