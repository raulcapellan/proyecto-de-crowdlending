import React, { Component, useState, useEffect } from "react";
import axios from "axios";
import { Esperando } from "./comunes";
import { useLocation, useParams } from 'react-router-dom';
import store from "./store/store";
import { Link } from "react-router-dom";
import MenuPromotor from "./menupromotor";


const withRouter = WrappedComponent => props => {
  const params = useParams();
  const { pathname } = useLocation()
  const match = { url: pathname, params: useParams() }

  return (
    <WrappedComponent
      {...props}
      params={params}
      match={match}
    />
  );
};

class ProyectosparaProm extends Component {
  constructor(props) {
    super(props);
    let pagina = props?.match?.params?.page ? props.match.params.page : 0;
    this.state = {
      modo: "list",
      listado: [],
      elemento: null,
      loading: true,
      pagina
    };
    this.idOriginal = null;
    this.url = "http://localhost:4200/promotor/proyectos/v1";
  }

  list() {
    this.setState({ loading: true });
    axios
      .get(`${this.url}`)
      .then(resp => {
        let paginas = resp.pages;
        let pagina = this.state.pagina < paginas ? this.state.pagina : paginas - 1;
        this.setState({ pagina, paginas });
        axios
          .get(`${this.url}`)
          .then(resp => {
            this.setState({
              modo: "list",
              listado: resp.data,
              loading: false
            });
          })
          .catch(err => {
            store.AddErrNotify(err);
            this.setState({ loading: false });
          });
      }).catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }

  add() {
    this.setState({
      modo: "add",
      elemento: { nombre: "", descripcion: "", nominal: "", interes: "", tipo: "", plazos: "" }
    });
  }

  edit(key) {
    this.setState({ loading: true });
    axios
      .get(this.url + `/${key}`)
      .then(resp => {
        this.setState({
          modo: "edit",
          elemento: resp.data,
          loading: false
        });
        this.idOriginal = key;
      })
      .catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }

  view(key) {
    this.setState({ loading: true });
    axios
      .get(this.url + `/${key}`)
      .then(resp => {
        this.setState({
          modo: "view",
          elemento: resp.data,
          loading: false
        });
      })
      .catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }

  delete(key) {
    if (!window.confirm("¿Seguro?")) return;
    this.setState({ loading: true });
    axios
      .delete(this.url + `/${key}`)
      .then(() => this.list())
      .catch(err => {
        store.AddErrNotify(err);
        this.setState({ loading: false });
      });
  }
  cancel() {
    this.list();
  }
  send(elemento) {
    // eslint-disable-next-line default-case
    switch (this.state.modo) {
      case "add":
        axios
          .post(this.url, elemento)
          .then(() => this.cancel())
          .catch(err => {
            store.AddErrNotify(err);
            this.setState({ loading: false });
          });
        break;
      case "edit":
        axios
          .put(this.url + `/${this.idOriginal}`, elemento)
          .then(() => this.cancel())
          .catch(err => {
            store.AddErrNotify(err);
            this.setState({ loading: false });
          });
        break;
    }
  }
  decodeRuta() {
    if (this.props.match.url === this.urlActual) return;
    this.urlActual = this.props.match.url;
    if (this.props.match.params.id) {
      if (this.props.match.url.endsWith('/edit'))
        this.edit(this.props.match.params.id);
      else
        this.view(this.props.match.params.id);
    } else {
      if (this.props.match.url.endsWith('/add'))
        this.add();
      else
        this.list();
    }
  }

  componentDidMount() {
    this.decodeRuta();
  }
  componentDidUpdate() {
    this.decodeRuta();
  }

  render() {
    if (this.state.loading) return <Esperando />;
    switch (this.state.modo) {
      case "add":
        return (
          <FormProyectos
            isAdd={this.state.modo === "add"}
            elemento={this.state.elemento}
            onCancel={e => this.cancel()}
            onSend={e => this.send(e)} />
        )
      case "edit":
        return (
          <FormProyectos
            isAdd={this.state.modo === "add"}
            elemento={this.state.elemento}
            onCancel={e => this.cancel()}
            onSend={e => this.send(e)}
          />
        );
      case "view":
        return (
          <VerProyectosProm
            detalle={this.state.detalle}
            isView={this.state.modo === "view"}
            elemento={this.state.elemento}
            onCancel={() => this.cancel()}
          />
        );

      default:
        return (
          <ListaProyectos
            listado={this.state.listado}
            onAdd={e => this.add()}
            onView={key => this.view(key)}
            onEdit={key => this.edit(key)}
            onDelete={key => this.delete(key)}
          />
        );
    }
  }
}
export class ListaProyectos extends Component {
  render() {
    return (
      <>
      <table className="table table-striped table-hover">
        <thead className="table-info">
          <tr>
            <th >Nombre del proyecto</th>
            <th >Tipo</th>
            <th>Estado</th>
            <th>Estado</th>
            <th>Nominal (€)</th>
            <th>
              <input
                type="button"
                className="btn btn-primary"
                value="Añadir"
                onClick={() => this.props.onAdd()}
              />
            </th>
          </tr>
        </thead>
        <tbody className="border border-light">
          {this.props.listado.map(item => (
            <tr key={item.id}>
              <td className="text-primary">
                {item.nombre}
              </td>
              <td className="text-success ">
                {item.tipo}
              </td>
              <td className="text-danger">
                {item.estado}
              </td>
              <td className="text-success ">
                {item.nominal}
              </td>
              <td>
                <Link to={"/ProyectosProm/" + item.id} type="button" className="btn btn-warning">
                  Ver detalle
                </Link>
                <input
                  type="button"
                  className="btn btn-success"
                  value="Editar"
                  onClick={e => this.props.onEdit(item.id)}
                />
                <input
                  type="button"
                  className="btn btn-danger"
                  value="Borrar"
                  onClick={e => this.props.onDelete(item.id)}
                />
                <br />
              </td>

            </tr>
          ))}
        </tbody>
      </table>
      </>
    );
  }
}

export function VerProyectosProm() {
  const [elemento, setElemento] = useState({});
  const url = "http://localhost:4200/promotor/proyectos/v1";
  const { id } = useParams();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    axios
      .get(url + `/${id}`)
      .then(resp => {
        setElemento(resp.data)
        setLoading(false);
      })
      .catch(err => {
        store.AddErrNotify(err);
        setLoading(false);
      });
  }, [id]);

  if (loading) return <Esperando />;
  return (

    <form >
      <p className="text-center">
        <img src={elemento.imagen} className="rounded-circle" alt="Foto del proyecto" />
      </p>
      <p className="text-center" >
        <br />
        <b>ID:</b> {elemento.id}
        <br />
        <b>Nombre del proyecto:</b> {elemento.nombre}
        <br />
        <b>Descrición del proyecto: </b> {elemento.descripcion}
        <br />
        <b>Nominal del préstamo: </b> {elemento.nominal}
        <br />
        <b>Tipo de interes: </b> {elemento.interes}
        <br />
        <b>Tipo de proyecto: </b> {elemento.tipo}
        <br />
        <b>Plazo de amortizacion: </b> {elemento.plazos}
        <br />
        <b>Rating: </b> {elemento.rating}
        <br />
        <b>Estado: </b> {elemento.estado}
        <br />
        <b>Creador: </b> {elemento.creador}
        <br />
        <b>Fecha de creación: </b> {elemento.creationDate}
        <br />
        <b>Fecha de modificación: </b> {elemento.modificationDate}
        <br />
      </p>
      <p>
        <Link type="button" className="btn btn-primary" to="/proyectos/" > Volver</Link>
      </p>
    </form>
  );
}
export class FormProyectos extends React.Component {
  constructor(props) {
    super(props);
    this.state = { elemento: props.elemento };
    this.form = React.createRef();
    this.handleChange = this.handleChange.bind(this);
    this.onSend = () => {
      if (this.props.onSend) this.props.onSend(this.state.elemento);
    };
    this.onCancel = () => {
      if (this.props.onCancel) this.props.onCancel();
    };
  }

  handleChange(event) {
    let cmp = event.target.name
    let valor = event.target.value
    this.setState(prev => {
      prev.elemento[cmp] = valor
      return { elemento: prev.elemento }
    });
  }

  render() {
    return (
      <>
        <form ref={tag => { this.form = tag }} >

          <div className="form-group col" >
            <div className="row">
              <div className="col">
                <label htmlFor='creador' value={this.state.elemento.creador} style={{ fontSize: 30, fontWeight: "bold" }}>Creador: </label>
                <input type="text" placeholder=" Escribe tu correo" className='form_control' onChange={this.handleChange} name="creador" style={{ display: 'flex', height: 45, width: 500, padding: 10, margin: 10, }} value={this.state.elemento.creador} required minLength={3} />
              </div>
              <div className="col">
                <label htmlFor='nombre' value={this.state.elemento.nombre} style={{ fontSize: 30, fontWeight: "bold" }}>Nombre del proyecto: </label>
                <input type="text" placeholder=" Escribe el nombre del proyecto" className='form_control' onChange={this.handleChange} name="nombre" style={{ display: 'flex', height: 45, width: 500, padding: 10, margin: 10, }} value={this.state.elemento.nombre} required minLength={3} />
              </div>
              <div className="col">
                <label htmlFor='descripcion' style={{ fontSize: 30, fontWeight: "bold" }} > Descrición del proyecto: </label>
                <textarea placeholder=" Minimo 20 caracteres" onChange={this.handleChange} type="textarea" name="descripcion" style={{ display: 'flex', margin: 10, paddingBottom: 1, boxSizing: 4, height: 55, width: 700, }} value={this.state.elemento.descripcion} required minLength={20} />
              </div>
            </div>
            <div className="col">
              <label htmlFor='nominal' style={{ fontSize: 30, fontWeight: "bold" }}> Nominal del préstamo(€): </label>
              <input placeholder="1000" type="number" name="nominal" onChange={this.handleChange} style={{ textAlign: 'center', display: 'flex', margin: 10, height: 45, fontSize: 23 }} value={this.state.elemento.nominal} required />
            </div>
            <br />
            <div className="form-group" style={{ fontSize: 30, fontWeight: "bold" }}>
              <label className="custom-select" required htmlFor="interes"> Elige el tipo de interés:</label>
              <select value={this.state.elemento.interes} onChange={this.handleChange} name="interes" placeholder='Opciones' label="interes" required>
                <option>Opciones</option>
                <option value="7">7%</option>
                <option value="9">9%</option>
                <option value="11">11%</option>
                <option value="13">13%</option>
              </select>
            </div>
            <br />
            <div className="form-group" style={{ fontSize: 30, fontWeight: "bold" }}>
              <label className="custom-select" required htmlFor="tipo"> Elige el tipo de proyecto:</label>
              <select value={this.state.elemento.tipo} onChange={this.handleChange} style={{ textAlign: 'center' }} name="tipo" placeholder='Tipos' label="tipo" required>
                <option>Tipos</option>
                <option value="Energías">Energías</option>
                <option value="Agrícola">Agrícola</option>
                <option value="Inmobiliario">Inmobiliario</option>
                <option value="Tecnología">Tecnología</option>
                <option value="Consumo">Consumo</option>
                <option value="Coche">Coche</option>
              </select>
            </div>
            <br />
            <div className="form-group" style={{ fontSize: 30, fontWeight: "bold" }}> Plazo de amortización:
              <input name="plazos" onChange={this.handleChange} type="number" value={this.state.elemento.plazos} style={{ textAlign: 'center' }} /> meses
            </div>
            <br />
            <div className="container-fluid" style={{ justifyContent: 'center', display: 'flex', width: '100%' }}>
              <button type="submit" className="btn btn-primary" onClick={this.onSend} >Enviar</button>
              <button type="submit" className="btn btn-dark " onClick={() => this.onCancel} >Volver</button>
            </div>
            <br />
          </div>

        </form>
      </>
    )
  }
}

export default withRouter(ProyectosparaProm);
