export function esNIF(nif) {
    if (!/^\d{1,8}[A-Za-z]$/.test(nif))
        return false;
    const letterValue = nif.substr(nif.length - 1).toUpperCase();
    const numberValue = nif.substr(0, nif.length - 1);
    return letterValue === 'TRWAGMYFPDXBNJZSQVHLCKE'.charAt(numberValue % 23);
}