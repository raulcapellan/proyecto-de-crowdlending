import React, {Component} from 'react';
import { ValidationMessageLogin } from './comunes';
import 'bootstrap-icons/font/bootstrap-icons.css';
import './imagenfondo.css'
import { Link } from "react-router-dom";
export default class FormularioLogin extends Component{
    constructor(props) {
        super(props)
        this.state = {
            elemento: {
                usuario: '',
                contraseña: '',
            },
            esInvalido: false,
            errores: {}
        }
        this.form = React.createRef();
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let cmp = event.target.name
        let value = event.target.value
        this.setState(prev => {
            let elemento = this.state.elemento;
            elemento[cmp] = value
            return { elemento }
        });
        this.validate()
    }
    
    componentDidMount() {
        this.validate()
    }
    validate() {
        if (this.form) {
            let esInvalido = false
            let errores = {}
            for (let cntr of this.form.current.elements) {
                if (cntr.name) {  
                    errores[cntr.name] = cntr.validationMessage;
                    if(!cntr.validity.valid) esInvalido = true
                }
            }
            this.setState({ errores, esInvalido });
        }
    }

    render() {
         return (
            <div className="bg">
            <form ref={this.form}>
                <h1 className="container-fluid row vh-100">
                    <h2 className="container">
                    <p className='display-3 text-center text-light'>
                    <i className="bi bi-cash-coin"  ></i>
                    </p>
                    <div className="col">
                    <input type='text' placeholder='Usuario' className= 'form-control' id='usuario' name='usuario' value={this.state.elemento.usuario} onChange={this.handleChange}
                        required/>
                        <small id="passwordHelpBlock" className="form-text text-muted">
                        <ValidationMessageLogin msg={this.state.errores.usuario} />
                        </small>
                    </div>
                    <br />
                    <div className="col">
                    <input type='password'placeholder='Contraseña' className= 'form-control' id='contraseña' name='contraseña' value={this.state.elemento.contraseña} onChange={this.handleChange}
                        required/>
                        <small id="passwordHelpBlock" className="form-text text-muted">
                        <ValidationMessageLogin msg={this.state.errores.contraseña} />
                        </small>
                    </div>
                    <br />
                    <input type="button" className="btn btn-light w-100" value="ACCEDER"/>
                    
                    </h2>
                    <p className="container-fluid">
                    <Link type="button" className="btn btn-secondary" to="/" > Volver</Link>
                    </p> 
                </h1>
                </form>
                </div>
                
        )
    }
}
