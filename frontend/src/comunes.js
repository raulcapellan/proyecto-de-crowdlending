import { Component, useState, useEffect, useRef } from "react";
import { NavLink } from 'react-router-dom';
import loading from './loading.gif'
import './comunes.css'
export class ValidationMessage extends Component {
    render() {
      if (this.props.msg) {
        return <output className="errorMsg">{this.props.msg}</output>;
      }
      return null;
    }
  }

  export class ValidationMessageLogin extends Component {
    render() {
      if (this.props.msg) {
        return <output className="errorMsgLogin">{this.props.msg}</output>;
      }
      return null;
    }
  }

export class Esperando extends Component {
    render() {
      return <div>
        <div className="ajax-wait"></div>
        <img className="ajax-wait" src={loading} alt="Cargando ..." />
      </div>;
    }
  }

export class ErrorBoundary extends Component {
    constructor(props) {
      super(props);
      this.state = { hasError: false, error: null, errorInfo: null };
    }
    static getDerivedStateFromError(error) {  // Actualiza el estado para que el siguiente renderizado lo muestre
      return { hasError: true };
    }
    componentDidCatch(error, info) {  // También puedes registrar el error en un servicio de reporte de errores
      this.setState({ hasError: true, error: error, errorInfo: info })
    }
    render() {
      if (this.state.hasError) { // Puedes renderizar cualquier interfaz de repuesto
        return <div>
          <h1>ERROR</h1>
          {this.state.error && <p>{this.state.error.toString()}</p>}
          {this.state.errorInfo && <p>{this.state.errorInfo.componentStack}</p>}
        </div>;
      }
      return this.props.children;
    }
  }
  
  export function Paginacion({ actual, total, url }) {
    let items = [];
    for (let number = 0; number < total; number++) {
        items.push(
            number === actual ?
                <li key={number} className="page-item active" aria-current="page"><NavLink to={url + number} className="page-link">{number + 1}</NavLink></li>
                :
                <li key={number} className="page-item"><NavLink to={url + number} className="page-link">{number + 1}</NavLink></li>
        );
    }
    return (
        <nav aria-label="Page navigation">
            <ul className="pagination">
                {items}
            </ul>
        </nav>
    )
  }
  