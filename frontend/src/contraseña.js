export function checkPassword(valor){
    var myregex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W).{8,}$/; 
   if(myregex.test(valor)){
       return true;        
   }else{
       return false;        
   }   
 }

export function checkForm(form){
    
    if(form.pwd1.value != "" && form.pwd1.value == form.pwd2.value) {
      if(!checkPassword(form.pwd1.value)) {
        alert("La contraseña no es valida!");
        form.pwd1.focus();
        return false;
      }
    } else {
      alert("Error: las contraseñas no coinciden!");
      form.pwd1.focus();
      return false;
    }
    return true;
  }