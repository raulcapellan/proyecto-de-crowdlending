import { fnValidateIBAN} from './IBAN.js'
import { esNIF} from './DNI'

describe('Pruebas del IBAN', () => {
    let validador;

    beforeEach(() => {
        validador = fnValidateIBAN
    });
    describe('Pruebas', () => {

        describe.each([
            ["ES6621000418401234567891", true],
            ["ES7848483834834834838383", false]
        ])('.validador(%s)', (a, expected) => {
            test(`returns ${expected}`, () => {
                expect(validador(a)).toBe(expected);
            });
        });

    });
});

describe('Pruebas del esNIF', () => {
    describe.each([
        '12345678z', '12345678Z', '1234S', '4g'
      ])('true (%s)', (nif) => {
        test(`true ${nif}`, () => {
          expect(esNIF(nif)).toBeTruthy()
        });
      });
      describe.each([
        '12345678', 'Z12345678', '1234J', null
      ])('false (%s)', (nif) => {
        test(`false ${nif}`, () => {
          expect(esNIF(nif)).toBeFalsy()
        });
      });
      
})
