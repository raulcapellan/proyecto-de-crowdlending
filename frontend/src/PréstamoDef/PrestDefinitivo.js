import './PrestDefinitivo.css';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Routes
} from "react-router-dom";
import CompInterestForm from './CompInteresForm.js'
import AmortizationForm from './AmortizationForm.js'

function PrestDefinitivo() {
  return (
    <div className="App">
      <title>
        Simulador de Préstamos
      </title>

      <meta name="Descripción" content="Calcula los intereses compuestos derivados del préstamo y muestra la tabla de amortización del mismo" />

      <Router>
        <div>
          <div className="navBarLinks">
            <Link to="/">Simulador amortizacion</Link>
            <br />
            <Link to="/amortization-calculadora">Calculadora de Amortización</Link>
            <hr />
          </div>

          <Routes>
            <Route exact path="/">
              <CompInterestForm />
            </Route>
            <Route path="/amortizacion-calculadora">
              <AmortizationForm />
            </Route>
          </Routes>
        </div>
      </Router>

    </div>
  );
}

export default PrestDefinitivo;