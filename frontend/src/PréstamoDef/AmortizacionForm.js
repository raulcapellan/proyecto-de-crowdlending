import React from "react"
import { useState } from "react"
import TablaAmortización from "./TablaAmortizacion.js"
import puntosCifras from './PuntosCifras.js'
import { Link } from "react-router-dom";
import "./simulador.css";


const FormularioAmortización = props => {

  const [nominalPréstamo, setnominalPréstamo] = useState("")
  const [tipoInterés, settipoInterés] = useState("")
  const [plazoAmortización, setplazoAmortización] = useState("")

  const cuantíaFinal = () => {
    let cuantíaFinal = 0
    // need to get interest rate per month as a decimal
    let tipoInterésCorregido = tipoInterés / 12 * 0.01
    let numeroMeses = plazoAmortización * 12

    if (nominalPréstamo && tipoInterés && plazoAmortización) {
      cuantíaFinal = nominalPréstamo * (tipoInterésCorregido * (1 + tipoInterésCorregido)**numeroMeses) / ((1 + tipoInterésCorregido)**numeroMeses - 1)
    }
    return cuantíaFinal
  }

  return (
    <div className="container-fluid simulador">
    <div className="container-fluid text-center">
      <div>
      <title>
        Calculadora de Amortización
      </title>

      <meta name="Descripción" content="Genera una tabla de amortización" />

      <h1 className="cool-font display-2">Calculadora de amortización</h1>
      <form>
        <label className="cool-font">
          Valor nominal del préstamo (€):&nbsp;
          <input className="cool-font" type="text" size="19" maxLength="18" value={nominalPréstamo} onChange={(e) => setnominalPréstamo(e.target.value)} />
        </label>
        <br />
        <br />
        <label className="cool-font">
          Tipo de interés:&nbsp;
          <input className="cool-font" type="text" size="6" maxLength="6" value={tipoInterés} onChange={(e) => settipoInterés(e.target.value)} />
          %
        </label>
        <br />
        <br />
        <label className="cool-font">
          Plazo de amortización (años):&nbsp;
          <input className="cool-font" type="text" size="2" maxLength="2" value={plazoAmortización} onChange={(e) => setplazoAmortización(e.target.value)} />
        </label>
      </form>
      </div>
      
      {cuantíaFinal() ? 
        <>
        <br />
        <br />

        <div className="cool-font">
          <h2>
            Para un préstamo a <span className="red">{plazoAmortización}</span> años con nominal&nbsp;<span className="red">{puntosCifras(nominalPréstamo)}€</span> 
            &nbsp;y un tipo de interés de <span className="red">{tipoInterés}%</span> la cuota de amortización sería de...
          </h2>
        </div>
    
        <h1 className="cool-big-font"><span className="red">{`${puntosCifras(Number.parseFloat(cuantíaFinal()).toFixed(2))}€`}</span></h1>

        <br />
        <br />
        <br />

        {
        <TablaAmortización nominalPréstamo={nominalPréstamo} tipoInterés={tipoInterés} plazoAmortización={plazoAmortización} cuotaMensual={cuantíaFinal()} />
        }
        </>
        : 
        <></>
      }
    <br></br>
    <br></br>
    </div>
    <Link type="button" className="btn btn-secondary" to="/" > Atrás</Link>
    </div>

  )
}

export default FormularioAmortización