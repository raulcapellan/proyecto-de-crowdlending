import React, { useState } from 'react'
import puntosCifras from './PuntosCifras.js'

const FormularioPréstamo = () => {

  const [nominalPréstamo, setnominalPréstamo] = useState("")
  const [tipoInterés, settipoInterés] = useState("")
  const [plazoAmortización, setplazoAmortización] = useState("")
  const [intervalo, setintervalo] = useState("anual")

  const cuantíaFinal = () => {
    let cuantíaFinal = 0
    if (nominalPréstamo && tipoInterés && plazoAmortización && intervalo) {
      if (intervalo === "anual") {
        cuantíaFinal = (nominalPréstamo * (1 + (tipoInterés * .01))**plazoAmortización)
      } else {
        cuantíaFinal = (nominalPréstamo * (1 + ((tipoInterés * .01) / 12))**(12 * plazoAmortización))
      }
    }
    return cuantíaFinal
  }

  return (
    <div>
      <title>
        Formulario de interés compuesto
      </title>

      <meta name="Description" content="Calcula el interés compuesto" />

      <h1 className="cool-font">Calculadora de interés compuesto</h1>
      <form>
          <label className="cool-font">
            Cuantía inicial:&nbsp;$
            <input className="cool-font" type="text" size="19" maxLength="18" value={nominalPréstamo} onChange={(e) => setnominalPréstamo(e.target.value)} />
          </label>
          <br />
          <br />
          <label className="cool-font">
            Tipo de interés:&nbsp;
            <input className="cool-font" type="text" size="6" maxLength="6" value={tipoInterés} onChange={(e) => settipoInterés(e.target.value)} />
            %
          </label>
          <br />
          <br />
          <label className="cool-font">
            Plazo de amortización:&nbsp;
            <input className="cool-font" type="text" size="2" maxLength="2" value={plazoAmortización} onChange={(e) => setplazoAmortización(e.target.value)} />
          </label>
          <br />
          <br />
          <label className="cool-font">
            Intervalo:&nbsp;
            <select className="cool-font" value={intervalo} onChange={(e) => setintervalo(e.target.value)}>
              <option className="cool-font" value="anual">anual</option>
              <option className="cool-font" value="monthly">monthly</option>
            </select>
          </label>
      </form>
    </div>
  )
}

export default FormularioPréstamo