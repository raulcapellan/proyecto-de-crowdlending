import React from "react"
import numberWithCommas from './PuntosCifras.js'
import './PrestDefinitivo.css';

const TablaAmortización = props => {
  let {nominalPréstamo, tipoInterés, plazoAmortización, cuotaMensual} = props

  // need to get interest rate per month as a decimal
  let tipoInterésCorregido = tipoInterés / 12 * 0.01

  // rounding function
  const dosDecimales= num => {
    return Number.parseFloat(num).toFixed(2)
  }

  const createSched = (nominalPréstamo, tipoInterésCorregido, plazoAmortización, cuotaMensual) => {
    let roundedcuotaMensual = dosDecimales(cuotaMensual)

    let balance = nominalPréstamo
    let rBalance = dosDecimales(balance)

    let interest
    let rInteres

    let principal
    let rPrincipal

    let CuotaFinal

    let arrayOfObjects = []

    let object

  
    for (let i = 1; i <= (plazoAmortización * 12) - 1; i++) {
      interest = rBalance * tipoInterésCorregido
      rInteres = dosDecimales(interest)

      principal = roundedcuotaMensual - rInteres
      rPrincipal = dosDecimales(principal)

      balance = balance - rPrincipal
      rBalance = dosDecimales(balance)

      object = {
        numeroPago: i,
        cuotaPago: roundedcuotaMensual,
        rInteres: rInteres,
        rPrincipal: rPrincipal,
        rBalance: rBalance
      }

      arrayOfObjects.push(object)

    }

    // get final interest
    interest = rBalance * tipoInterésCorregido
    rInteres = dosDecimales(interest)

    // get final principal
    principal = rBalance
    rPrincipal = dosDecimales(principal)

    // get final payment amount
    CuotaFinal = dosDecimales(Number.parseFloat(rPrincipal) + Number.parseFloat(rInteres))

    // update rounded balance
    rBalance = rBalance - rPrincipal

    object = {
      numeroPago: plazoAmortización * 12,
      cuotaPago: CuotaFinal,
      rInteres: rInteres,
      rPrincipal: rPrincipal,
      rBalance: dosDecimales(rBalance)
    }

    arrayOfObjects.push(object)

    return arrayOfObjects.map((obj, numeroPago) => {
      return (
        <tr key={numeroPago}>
          <td>{numberWithCommas(obj.numeroPago)}</td>
          <td>{numberWithCommas(obj.cuotaPago)}€</td>
          <td>{numberWithCommas(obj.rInteres)}€</td>
          <td>{numberWithCommas(obj.rPrincipal)}€</td>
          <td>{numberWithCommas(obj.rBalance)}€</td>
        </tr>
      )
    })
  }

  return (
    <>
      <h1 className="cool-font">Tabla de Amortización</h1>
        <table id="amor-table">
          <tbody>
            <tr>
              <th>Mensualidad.</th>
              <th>Cuota de Amortización</th>
              <th>Intereses</th>
              <th>Principal</th>
              <th>Amortizable</th>
            </tr>
            {createSched(nominalPréstamo, tipoInterésCorregido, plazoAmortización, cuotaMensual)}
          </tbody>
        </table>
    </>
  )
} 

export default TablaAmortización