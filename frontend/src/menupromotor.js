import { Link } from "react-router-dom";
import "./inicio.css";
function MenuPromotor(props) {
  return (
    <>
        <Link className="navbar-brand" to="/bdpromotoresProm">
          Área promotores
        </Link>
        <Link className="navbar-brand" to="/proyectos">
          Proyectos
        </Link>
        <Link className="navbar-brand" to="/simulador">
          Simulador de Préstamos
        </Link>
    </>
  );
}

export default MenuPromotor;
