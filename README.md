# Proyecto: Crowdlending

## Participantes

- Marta Blanquer Valderas
- Jaime Moncada del Val
- Raúl Sánchez Capellán

**Descripción de la página**:

La aplicación de Crowdlending de Crowd-Wealth permite cubrir las necesidades de los proyectos creados por promotores gracias a la financiación proporcionada por los inversores, dos figuras que quedarán comunicadas a través del gestor de la plataforma, quien se encarga de realizar la gestión de los datos y pagos que tienen lugar en la plataforma.

Entre las principales funciones correspondientes a la aplicación se encuentran:

-Creación de proyectos de diversa índole. Los usuarios con perfil de promotor podrán definir su cartera con uno o varios proyectos.

-Solicitud y estudio de viabilidad para la financiación de los proyectos generados por los promotores.

-* Provisional (Gestión de pagos por parte del gestor de la plataforma)
 
-Evaluación del perfil crediticio de promotores y perfil de compromiso de los inversores por parte del gestor de la plataforma.

-Creación de cartera de inversiones en los distintos proyectos para los usuarios con perfil de inversores.

-Apartado de comentarios de calificación para inversores y promotores.

  
